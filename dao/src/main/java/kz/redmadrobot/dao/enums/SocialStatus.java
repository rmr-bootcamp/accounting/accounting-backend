package kz.redmadrobot.dao.enums;

public enum SocialStatus {
    PENSIONER,
    HANDICAPPED
}
