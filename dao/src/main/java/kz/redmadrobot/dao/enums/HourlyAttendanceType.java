package kz.redmadrobot.dao.enums;

public enum HourlyAttendanceType {
    OVERTIME,
    LATE_FINE
}
