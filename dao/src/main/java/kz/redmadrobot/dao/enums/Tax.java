package kz.redmadrobot.dao.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@AllArgsConstructor
@Getter
//TODO добавить паттерн builder
public enum Tax {

    VOSMS("VOSMS", BigDecimal.valueOf(0.02), true, BigDecimal.valueOf(14000), null),

    OPV("OPV", BigDecimal.valueOf(0.10), true, BigDecimal.valueOf(350000), null),

    IPN("IPN", BigDecimal.valueOf(0.10), true, null, null),

    SO("SO", BigDecimal.valueOf(0.035), false, BigDecimal.valueOf(17150), BigDecimal.valueOf(2450)),

    OSMS("OSMS", BigDecimal.valueOf(0.03), false, BigDecimal.valueOf(21000), null);


    private final String name;
    private final BigDecimal interestRate;
    private final boolean isEmployeeTaxPayer;
    private final BigDecimal maxAmountToWithdraw;
    private final BigDecimal minAmountToWithdraw;
}
