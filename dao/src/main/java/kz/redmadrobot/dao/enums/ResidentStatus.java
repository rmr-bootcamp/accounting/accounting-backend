package kz.redmadrobot.dao.enums;

public enum ResidentStatus {
    RESIDENT,
    NON_RESIDENT
}
