package kz.redmadrobot.dao.entity;

import jakarta.persistence.*;
import kz.redmadrobot.dao.enums.TokenType;
import lombok.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tokens")
public class Token {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long id;

  @Column(unique = true)
  public String token;

  @Enumerated(EnumType.STRING)
  public TokenType tokenType = TokenType.BEARER;

  public boolean revoked;

  public boolean expired;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "auth_details_id")
  public AuthDetails authDetails;
}
