package kz.redmadrobot.dao.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "auth_details")
public class AuthDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "common_name", nullable = false)
    private String commonName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "organization", nullable = false)
    private String organization;

    @Column(name = "gender", nullable = false)
    private String gender;

    @Column(name = "iin", nullable = false)
    private String iin;

    @Column(name = "bin", nullable = false)
    private String bin;

    @Column(name = "country", nullable = false)
    private String country;

    @Column(name = "locality", nullable = false)
    private String locality;

    @Column(name = "state", nullable = false)
    private String state;

    @OneToMany(mappedBy = "authDetails")
    private List<Token> tokens;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        AuthDetails that = (AuthDetails) o;
        return getUsername() != null && Objects.equals(getUsername(), that.getUsername());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
