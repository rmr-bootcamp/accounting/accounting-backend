package kz.redmadrobot.dao.entity;

import jakarta.persistence.*;

import java.util.Objects;
import java.util.Set;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cascade;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "companies")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "company_name", nullable = false)
    private String companyName;

    @Column(name = "corporate_email", nullable = false)
    private String corporateEmail;

    @Column(name = "legal_form", nullable = false)
    private String legalForm;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "auth_details_id")
    private AuthDetails authDetails;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Company companies = (Company) o;
        return getId() != null && Objects.equals(getId(), companies.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
