package kz.redmadrobot.dao;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootConfiguration
@EntityScan(basePackages = "kz.redmadrobot.dao.entity")
@EnableJpaRepositories(basePackages = "kz.redmadrobot.dao.repository")
public class DaoConfig {
}