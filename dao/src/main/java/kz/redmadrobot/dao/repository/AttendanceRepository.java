package kz.redmadrobot.dao.repository;

import kz.redmadrobot.dao.entity.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public interface AttendanceRepository extends JpaRepository<Attendance, Long> {
    @Query("select atn from Attendance atn where atn.reportedEndDate >= :startDate and atn.reportedStartDate <= :endDate and atn.employee.id = :employeeId")
    List<Attendance> findAttendanceByDateAndEmployeeId(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate, @Param("employeeId") Long employeeId);

    @Query( """
                SELECT id FROM Attendance atn
                WHERE atn.employee.id = :employeeId
            """
    )
    LinkedList<Long> getIdsByEmployeeId(@Param("employeeId") Long employeeId);
}