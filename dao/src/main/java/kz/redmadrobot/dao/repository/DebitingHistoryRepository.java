package kz.redmadrobot.dao.repository;

import kz.redmadrobot.dao.entity.DebitingHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;

public interface DebitingHistoryRepository extends JpaRepository<DebitingHistory, Long> {
    DebitingHistory findByEmployeeId(Long employeeId);
}