package kz.redmadrobot.dao.repository;

import kz.redmadrobot.dao.entity.AuthDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthDetailsRepository extends JpaRepository<AuthDetails, Long> {
    Optional<AuthDetails> findByEmail(String email);
}