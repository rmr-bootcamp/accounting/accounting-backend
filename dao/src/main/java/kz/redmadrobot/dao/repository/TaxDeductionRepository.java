package kz.redmadrobot.dao.repository;

import kz.redmadrobot.dao.entity.TaxDeduction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaxDeductionRepository extends JpaRepository<TaxDeduction, Long> {
}