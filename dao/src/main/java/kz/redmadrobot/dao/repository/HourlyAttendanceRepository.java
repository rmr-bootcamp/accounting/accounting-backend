package kz.redmadrobot.dao.repository;

import kz.redmadrobot.dao.entity.Employee;
import kz.redmadrobot.dao.entity.HourlyAttendance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

public interface HourlyAttendanceRepository extends JpaRepository<HourlyAttendance, Long> {
    @Query("select hatn from HourlyAttendance hatn where hatn.reportedDate = :reportedDate and hatn.employee.id = :employeeId")
    Optional<HourlyAttendance> findHourlyAttendanceByDateAndEmployeeId(@Param("reportedDate") LocalDate reportedDate, @Param("employeeId") Long employeeId);

    @Modifying
    @Query("delete from HourlyAttendance hatn where hatn.reportedDate = :reportedDate and hatn.employee.id = :employeeId")
    void deleteHourlyAttendanceByEmployeeId(@Param("reportedDate") LocalDate reportedDate, @Param("employeeId") Long employeeId);

    void deleteByEmployeeId(Long employeeId);

    Set<HourlyAttendance> findByEmployeeAndReportedDateBetween(Employee employee, LocalDate startDate, LocalDate endDate);
}
