package kz.redmadrobot.dao.repository;

import kz.redmadrobot.dao.entity.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;

public interface CompanyRepository extends JpaRepository<Company, Long> {
    @Query("select c from Company c where lower(c.legalForm) = lower(:legalForm) and lower(c.authDetails.email) = lower(:email)")
    Page<Company> findByLegalFormIgnoreCaseAndAuthDetailsEmail(String email, @NonNull String legalForm, Pageable pageable);
    boolean existsByCorporateEmailIgnoreCase(@NonNull String corporateEmail);
    boolean existsByCompanyNameIgnoreCase(@NonNull String companyName);

}