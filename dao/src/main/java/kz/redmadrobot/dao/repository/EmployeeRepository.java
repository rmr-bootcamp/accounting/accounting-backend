package kz.redmadrobot.dao.repository;

import kz.redmadrobot.dao.entity.Company;
import kz.redmadrobot.dao.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    boolean existsByEmail(String email);
    Page<Employee> findAllByCompanyId(Long companyId, Pageable pageable);


    @Query("""
                SELECT emp FROM Employee emp
                WHERE emp.company = :company AND year(emp.hiredAt) <= :year AND month(emp.hiredAt) <= :month
            """)
    List<Employee> findCompanyEmployeesHiredBeforeMonthAndYear(Company company, Integer year, Integer month);
}