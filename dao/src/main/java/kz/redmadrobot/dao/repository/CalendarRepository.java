package kz.redmadrobot.dao.repository;

import kz.redmadrobot.dao.entity.Calendar;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface CalendarRepository extends JpaRepository<Calendar, Long> {

    List<Calendar> findByDateBetween(LocalDate start, LocalDate end);
}
