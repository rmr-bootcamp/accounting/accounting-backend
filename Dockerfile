FROM eclipse-temurin:17

LABEL mentainer = "amirkenesbay@gmail.com"

WORKDIR /app

COPY api/build/libs/api-0.0.1-SNAPSHOT.jar /app/api.jar

ENTRYPOINT ["java", "-jar", "api.jar"]