insert into calendar(id, name, date_type, date)
    values (1, 'Day of the unity of peoples', 'HOLIDAY', '2023-05-01');
insert into calendar(id, name, date_type, date)
    values (2, 'Fatherland Defender''s day', 'HOLIDAY', '2023-05-07');
insert into calendar(id, name, date_type, date)
    values (3, 'Fatherland Defender''s day', 'HOLIDAY', '2023-05-08');
insert into calendar(id, name, date_type, date)
    values (4, 'Victory day', 'HOLIDAY', '2023-05-09');

insert into calendar(id, name, date_type, date)
    values (5, 'First day of Kurban Ait', 'HOLIDAY', '2023-06-28');

insert into calendar(id, name, date_type, date)
    values (6, 'Capital day', 'FORCED_WORKDAY', '2023-07-01');
insert into calendar(id, name, date_type, date)
    values (7, 'Capital day', 'HOLIDAY', '2023-07-06');
insert into calendar(id, name, date_type, date)
    values (8, 'Capital day', 'HOLIDAY', '2023-07-07');
