package kz.redmadrobot.api.security.service;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import kz.redmadrobot.business.service.impl.TokenServiceImpl;
import kz.redmadrobot.dao.entity.Token;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
@Service
@RequiredArgsConstructor
public class LogoutService implements LogoutHandler {
    private final TokenServiceImpl tokenService;

    @Override
    public void logout(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication
    ) {
        final String authHeader = request.getHeader(AUTHORIZATION);
        final String jwt;
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            return;
        }
        jwt = authHeader.substring(7);
        Optional<Token> storedToken = tokenService.getOneByToken(jwt);
        if(storedToken.isPresent()) {
            SecurityContextHolder.clearContext();
        }
    }
}
