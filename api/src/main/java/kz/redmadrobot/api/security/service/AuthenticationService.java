package kz.redmadrobot.api.security.service;

import kz.redmadrobot.api.security.config.filter.JwtService;
import kz.redmadrobot.api.security.details.AuthenticationDetails;
import kz.redmadrobot.business.exception.registration.UserIsNotValidException;
import kz.redmadrobot.business.model.dto.AuthDetailsDto;
import kz.redmadrobot.business.model.dto.SignersDto;
import kz.redmadrobot.business.model.request.AccessTokenCreateRequest;
import kz.redmadrobot.business.model.request.LoginRequest;
import kz.redmadrobot.business.model.request.RegistrationRequest;
import kz.redmadrobot.business.model.request.TokenCreateRequest;
import kz.redmadrobot.business.model.response.AccessTokenResponse;
import kz.redmadrobot.business.model.response.TokenResponse;
import kz.redmadrobot.business.service.impl.AuthDetailsServiceImpl;
import kz.redmadrobot.business.service.impl.TokenServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final AuthDetailsServiceImpl authDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationDetails authenticationDetails;
    private final AuthenticationManager authenticationManager;
    private final TokenServiceImpl tokenService;

    public TokenResponse register(RegistrationRequest registrationRequest, SignersDto signersDto) {
        List<String> keyUsers = signersDto.signers().get(0).keyUser();
        if (keyUsers.contains("CEO") || keyUsers.contains("CAN_SIGN_FINANCIAL")) {
            // Encode username password
            registrationRequest.setPassword(passwordEncoder.encode(registrationRequest.getPassword()));

            // Create username and set the data into custom UserDetails object
            AuthDetailsDto authDetailsDto = authDetailsService.create(registrationRequest, signersDto);
            authenticationDetails.setAuthDetailsDto(authDetailsDto);

            // Generate access and refresh tokens by user details
            String jwtAccessToken = jwtService.generateAccessToken(authenticationDetails);
            String jwtRefreshToken = jwtService.generateRefreshToken(authenticationDetails);

            TokenCreateRequest tokenCreateRequest = getToken(jwtAccessToken, authDetailsDto);

            // Save tokens information from request object
            tokenService.saveUserToken(tokenCreateRequest);

            return setTokens(jwtAccessToken, jwtRefreshToken);
        }
        throw new UserIsNotValidException("User is not valid");
    }

    public TokenResponse login(LoginRequest loginRequest) {
        // Authenticate user by their email and password
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );

        // Search user by their email and set the data into custom UserDetails object
        AuthDetailsDto authDetailsDto = authDetailsService.getOneByEmail(loginRequest.getEmail());
        authenticationDetails.setAuthDetailsDto(authDetailsDto);

        // Generate access and refresh tokens by user details
        var jwtAccessToken = jwtService.generateAccessToken(authenticationDetails);
        var jwtRefreshToken = jwtService.generateRefreshToken(authenticationDetails);

        TokenCreateRequest tokenCreateRequest = getToken(jwtAccessToken, authDetailsDto);

        // Update the user token statuses to revoked and expired
        tokenService.revokeAllUserTokens(tokenCreateRequest);

        // Save tokens information from request object
        tokenService.saveUserToken(tokenCreateRequest);

        return setTokens(jwtAccessToken, jwtRefreshToken);
    }

    public AccessTokenResponse refresh(AccessTokenCreateRequest accessTokenCreateRequest) {
        // Extract user's email from refresh token
        String extractedEmailFromRefreshTokenClaim = jwtService.extractUsername(accessTokenCreateRequest.getRefreshToken());

        // Search user by their email and set the data into custom UserDetails object
        AuthDetailsDto authDetailsDto = authDetailsService.getOneByEmail(extractedEmailFromRefreshTokenClaim);
        authenticationDetails.setAuthDetailsDto(authDetailsDto);

        // Generate new access token
        var jwtAccessToken = jwtService.generateAccessToken(authenticationDetails);

        TokenCreateRequest tokenCreateRequest = getToken(jwtAccessToken, authDetailsDto);

        // Update the user token statuses to revoked and expired
        tokenService.revokeAllUserTokens(tokenCreateRequest);

        // Save tokens information from request object
        tokenService.saveUserToken(tokenCreateRequest);

        // Return new access token
        return AccessTokenResponse.builder()
                .accessToken(jwtAccessToken)
                .build();
    }

    private TokenCreateRequest getToken(String jwtAccessToken, AuthDetailsDto authDetailsDto) {
        TokenCreateRequest tokenCreateRequest = new TokenCreateRequest();
        tokenCreateRequest.setToken(jwtAccessToken);
        tokenCreateRequest.setAuthDetailsDto(authDetailsDto);
        return tokenCreateRequest;
    }

    private TokenResponse setTokens(String jwtAccessToken, String jwtRefreshToken) {
        return TokenResponse.builder()
                .accessToken(jwtAccessToken)
                .refreshToken(jwtRefreshToken)
                .build();
    }
}
