package kz.redmadrobot.api.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import kz.redmadrobot.business.model.dto.SignersDto;
import lombok.Getter;
import org.springframework.stereotype.Service;

@Service
@Getter
public class SignerUtil {
    private SignersDto signersDto;
    public SignersDto saveSignerToDto(String edsResponse) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        this.signersDto = objectMapper.readValue(edsResponse, SignersDto.class);
        return signersDto;
    }
}
