package kz.redmadrobot.api.util;

import kz.redmadrobot.business.model.request.ElectronicDigitalSignatureXmlDtoRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(value = "edsSign", url = "http://176.9.24.125:12303/xml/sign")
@Service
public interface ElectronicDigitalSignatureSignerUtil {
    @PostMapping
    String getSign(ElectronicDigitalSignatureXmlDtoRequest electronicDigitalSignatureXmlDtoRequest);
}
