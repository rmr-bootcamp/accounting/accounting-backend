package kz.redmadrobot.api;

import kz.redmadrobot.business.BusinessConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackageClasses = {SalaryTimeApplication.class, BusinessConfig.class})
@EnableFeignClients
public class SalaryTimeApplication {
    public static void main(String[] args) {
        SpringApplication.run(SalaryTimeApplication.class, args);
    }
}