package kz.redmadrobot.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import kz.redmadrobot.api.model.ApiErrorResponse;
import kz.redmadrobot.business.model.dto.AttendanceDto;
import kz.redmadrobot.business.model.dto.CompanyDto;
import kz.redmadrobot.business.model.request.AttendanceCreateRequest;
import kz.redmadrobot.business.service.AttendanceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RequestMapping("/${api.version}/employee/{employeeId}/attendance")
@Tag(name = "Attendance", description = "Attendance related resources")
public class AttendanceController {
    private final AttendanceService attendanceService;

    @PostMapping
    @Operation(summary = "Create attendance", description = "Creating attendance and unique id assigning. Follow model's " +
            "constrains to avoid unhandled errors")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Attendance created and will be returned with id", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = CompanyDto.class))})
    })
    public ResponseEntity<AttendanceDto> create(@PathVariable(value = "employeeId") Long id,
                                                @Valid @RequestBody AttendanceCreateRequest attendanceCreateRequest) {
        return ResponseEntity.status(HttpStatus.CREATED).body(attendanceService.create(id, attendanceCreateRequest));
    }

    @GetMapping("/{attendanceId}")
    @Operation(summary = "Get attendance", description = "Searching for one attendance by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Attendance found and retrieved", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AttendanceDto.class))}),
            @ApiResponse(responseCode = "404", description = "Attendance not found", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public ResponseEntity<AttendanceDto> getAttendanceByEmployeeId(@PathVariable(value = "employeeId") Long employeeId,
                                                                   @PathVariable(value = "attendanceId") Long attendanceId) {
        return ResponseEntity.ok(attendanceService.getOne(employeeId, attendanceId));
    }

    @GetMapping
    @Operation(summary = "Get attendances", description = "Retrieving all attendances page by page and with capability to sort")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Request retrieved the list of attendances or empty list", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AttendanceDto.class))}),
            @ApiResponse(responseCode = "400", description = "One of the parameters is invalid", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public ResponseEntity<List<AttendanceDto>> getAllByPage(@RequestParam(name = "startDate", defaultValue = "2023-03-15") String startDate,
                                                            @RequestParam(name = "endDate", defaultValue = "2023-03-20") String endDate,
                                                            @PathVariable Long employeeId) {
        return ResponseEntity.ok(attendanceService.getAttendanceByDate(startDate, endDate, employeeId));
    }

    @PutMapping("/{attendanceId}")
    @Operation(summary = "Put attendance", description = "Updating for one attendance by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Attendance updated", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AttendanceDto.class))}),
            @ApiResponse(responseCode = "404", description = "Attendance not found", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public ResponseEntity<AttendanceDto> update(@PathVariable(value = "employeeId") Long employeeId,
                                                                   @PathVariable(value = "attendanceId") Long attendanceId,
                                                @Valid @RequestBody AttendanceCreateRequest attendanceCreateRequest) {
        return ResponseEntity.ok(attendanceService.update(employeeId, attendanceId, attendanceCreateRequest));
    }

    @DeleteMapping("{attendanceId}")
    @Operation(summary = "Delete attendance", description = "Delete attendance by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Attendance deleted"),
            @ApiResponse(responseCode = "404", description = "Attendance not found", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public ResponseEntity<HttpStatus> delete(@PathVariable Long employeeId, @PathVariable Long attendanceId) {
        attendanceService.deleteById(employeeId, attendanceId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
