package kz.redmadrobot.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import kz.redmadrobot.api.model.ApiErrorResponse;
import kz.redmadrobot.business.model.dto.EmployeeDto;
import kz.redmadrobot.business.model.enums.sort.EmployeeSortBy;
import kz.redmadrobot.business.model.enums.sort.SortType;
import kz.redmadrobot.business.model.request.EmployeeCreateRequest;
import kz.redmadrobot.business.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/${api.version}/employees")
@CrossOrigin(origins = "*")
@Tag(name = "Employee", description = "Employee related resources")
public class EmployeeController {

    private final EmployeeService employeeService;

    @PostMapping
    @Operation(summary = "Create employee", description = "Creating employee and unique id assigning. Follow model's " +
                                                                                "constraints to avoid unhandled errors")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Employee created and will be returned with id", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = EmployeeDto.class))}),
            @ApiResponse(responseCode = "409", description = "Employee email is already taken", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public ResponseEntity<EmployeeDto> create(@Valid @RequestBody EmployeeCreateRequest employeeCreateRequest) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(employeeService.create(employeeCreateRequest));
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get Employee", description = "Searching for one employee by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Employee found and retrieved", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = EmployeeDto.class))}),
            @ApiResponse(responseCode = "404", description = "Employee not found", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public ResponseEntity<EmployeeDto> getOne(@PathVariable Long id) {
        return ResponseEntity.ok(employeeService.getOne(id));
    }

    @GetMapping
    @Operation(summary = "Get Employees", description = "Retrieving all employees page by page and with capability to sort")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Request retrieved the list of employees or empty list", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = EmployeeDto.class))}),
            @ApiResponse(responseCode = "400", description = "One of the parameters is invalid", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public ResponseEntity<Set<EmployeeDto>> getAllByPage(@RequestParam(defaultValue = "0") @PositiveOrZero Integer page,
                                                         @RequestParam(defaultValue = "10") @Positive Integer quantity,
                                                         @RequestParam(defaultValue = "ID") EmployeeSortBy employeeSortBy,
                                                         @RequestParam(defaultValue = "NONE") SortType sortType,
                                                         @RequestParam(defaultValue = "companyId") Long companyId) {
        return ResponseEntity.ok(employeeService.getAllByPage(page, quantity, employeeSortBy, sortType, companyId));
    }


    @DeleteMapping("{id}")
    @Operation(summary = "Delete employee", description = "Delete employee by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Employee deleted"),
            @ApiResponse(responseCode = "404", description = "Employee not found", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public ResponseEntity<HttpStatus> delete(@PathVariable Long id) {
        employeeService.deleteById(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
