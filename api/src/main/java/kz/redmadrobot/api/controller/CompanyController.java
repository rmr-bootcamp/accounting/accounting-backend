package kz.redmadrobot.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import kz.redmadrobot.api.model.ApiErrorResponse;
import kz.redmadrobot.api.security.util.SecurityUtil;
import kz.redmadrobot.business.model.dto.CompanyDto;
import kz.redmadrobot.business.model.enums.LegalForm;
import kz.redmadrobot.business.model.enums.sort.CompanySortBy;
import kz.redmadrobot.business.model.enums.sort.SortType;
import kz.redmadrobot.business.model.request.CompanyCreateRequest;
import kz.redmadrobot.business.service.CompanyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RequestMapping("/${api.version}/companies")
@Tag(name = "Company", description = "Company related resources")
@Slf4j
public class CompanyController {

    private final CompanyService companyService;

    @PostMapping
    @Operation(summary = "Create company", description = "Creating company and unique id assigning. Follow model's " +
            "constrains to avoid unhandled errors")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Company created and will be returned with id", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = CompanyDto.class))}),
            @ApiResponse(responseCode = "409", description = "Company name or email is already taken", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public ResponseEntity<CompanyDto> create(@Valid @RequestBody CompanyCreateRequest companyCreateRequest) {
        String userEmail = SecurityUtil.getAuthenticatedUser();
        companyCreateRequest.setUserEmail(userEmail);
        return ResponseEntity.status(HttpStatus.CREATED).body(companyService.create(companyCreateRequest));
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get Company", description = "Searching for one company by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Company found and retrieved", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = CompanyDto.class))}),
            @ApiResponse(responseCode = "404", description = "Company not found", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public ResponseEntity<CompanyDto> getOne(@PathVariable Long id) {
        return ResponseEntity.ok(companyService.getOne(id));
    }

    @GetMapping
    @Operation(summary = "Get Companies", description = "Retrieving all companies page by page and with capability to sort")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Request retrieved the list of companies or empty list", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = CompanyDto.class))}),
            @ApiResponse(responseCode = "400", description = "One of the parameters is invalid", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public ResponseEntity<Set<CompanyDto>> getAllByPage(@RequestParam(defaultValue = "0") @PositiveOrZero Integer page,
                                                        @RequestParam(defaultValue = "10") @Positive Integer quantity,
                                                        @RequestParam(defaultValue = "ID") CompanySortBy companySortBy,
                                                        @RequestParam(defaultValue = "ASC") SortType sortType) {
        return ResponseEntity.ok(companyService.getAllByPage(page, quantity, companySortBy, sortType));
    }

    @GetMapping("/legal-form")
    @Operation(summary = "Get Companies by legal form", description = "Retrieving companies page by page and with " +
            "capability to sort and filtered by legal form.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Request retrieved the list of companies filtered by lf or empty list", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = CompanyDto.class))}),
            @ApiResponse(responseCode = "400", description = "One of the parameters is invalid", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public ResponseEntity<Set<CompanyDto>> getByLegalForm(@RequestParam(name = "lf") LegalForm legalForm,
                                                          @RequestParam(defaultValue = "0") @PositiveOrZero Integer page,
                                                          @RequestParam(defaultValue = "10") @Positive Integer quantity,
                                                          @RequestParam(defaultValue = "ID") CompanySortBy companySortBy,
                                                          @RequestParam(defaultValue = "ASC") SortType sortType) {
        String userEmail = SecurityUtil.getAuthenticatedUser();
        return ResponseEntity.ok(companyService.getAllByLegalForm(userEmail, legalForm, page, quantity, companySortBy, sortType));
    }
}