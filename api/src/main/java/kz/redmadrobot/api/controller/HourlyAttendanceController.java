package kz.redmadrobot.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import kz.redmadrobot.api.model.ApiErrorResponse;
import kz.redmadrobot.business.model.dto.HourlyAttendanceDto;
import kz.redmadrobot.business.model.request.HourlyAttendanceCreateRequest;
import kz.redmadrobot.business.service.impl.HourlyAttendanceServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/${api.version}/employee/{employeeId}/hourly-attendances")
@CrossOrigin(origins = "*")
@Tag(name = "Hourly attendance", description = "Hourly attendance related resources")
public class HourlyAttendanceController {
    private final HourlyAttendanceServiceImpl hourlyAttendanceService;

    @PostMapping
    @Operation(summary = "Create hourly attendance", description = "Creating hourly attendance and unique id assigning. Follow model's " +
            "constrains to avoid unhandled errors")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Hourly attendance created and will be returned with id", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = HourlyAttendanceDto.class))})
    })
    public ResponseEntity<HourlyAttendanceDto> create(@PathVariable(value = "employeeId") Long id,
                                                      @Valid @RequestBody HourlyAttendanceCreateRequest hourlyAttendanceCreateRequest) {
        return ResponseEntity.status(HttpStatus.CREATED).body(hourlyAttendanceService.create(id, hourlyAttendanceCreateRequest));
    }

    @GetMapping("/{hourlyAttendanceId}")
    @Operation(summary = "Get hourly attendance", description = "Searching for one hourly attendance by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Hourly attendance found and retrieved", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = HourlyAttendanceDto.class))}),
            @ApiResponse(responseCode = "404", description = "Hourly attendance not found", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public HourlyAttendanceDto getHourlyAttendanceByEmployeeId(@PathVariable(value = "employeeId") Long employeeId,
                                                               @PathVariable(value = "hourlyAttendanceId") Long hourlyAttendanceId) {
        return hourlyAttendanceService.getOne(employeeId, hourlyAttendanceId);
    }

    @GetMapping
    @Operation(summary = "Get hourly attendance", description = "Searching for one hourly attendance by reported date")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Hourly attendance found and retrieved", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = HourlyAttendanceDto.class))}),
            @ApiResponse(responseCode = "404", description = "Hourly attendance not found", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public HourlyAttendanceDto getHourlyAttendanceByDateAndEmployeeId(@PathVariable(value = "employeeId") Long employeeId,
                                                                      @RequestParam(name = "reportedDate", defaultValue = "2023-04-07") String reportedDate) {
        return hourlyAttendanceService.getByDate(employeeId, reportedDate);
    }

    @PutMapping
    @Operation(summary = "Put hourly attendance", description = "Updating for one hourly attendance by its reported date")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Hourly attendance updated", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = HourlyAttendanceDto.class))}),
            @ApiResponse(responseCode = "404", description = "Hourly attendance not found", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public HourlyAttendanceDto update(@PathVariable(value = "employeeId") Long employeeId,
                                      @Valid @RequestBody HourlyAttendanceCreateRequest hourlyAttendanceCreateRequest) {
        return hourlyAttendanceService.update(employeeId, hourlyAttendanceCreateRequest);
    }

    @DeleteMapping
    @Operation(summary = "Delete hourly attendance", description = "Updating for one hourly attendance by its reported date")
    public String deleteById(@PathVariable Long employeeId) {
        return hourlyAttendanceService.deleteByEmployeeId(employeeId);
    }

    @DeleteMapping("/date")
    @Operation(summary = "Delete hourly attendance", description = "Updating for one hourly attendance by its reported date")
    public String deleteByDate(@RequestParam(name = "reportedDate", defaultValue = "2023-04-07") String reportedDate,
                               @PathVariable Long employeeId) {
        return hourlyAttendanceService.deleteByDate(reportedDate, employeeId);
    }
}
