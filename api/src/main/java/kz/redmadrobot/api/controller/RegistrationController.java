package kz.redmadrobot.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import kz.redmadrobot.api.model.ApiErrorResponse;
import kz.redmadrobot.api.security.service.AuthenticationService;
import kz.redmadrobot.api.util.ElectronicDigitalSignatureSignerUtil;
import kz.redmadrobot.api.util.SignerUtil;
import kz.redmadrobot.business.model.dto.SignersDto;
import kz.redmadrobot.business.model.request.ElectronicDigitalSignatureXmlDtoRequest;
import kz.redmadrobot.business.model.request.RegistrationRequest;
import kz.redmadrobot.business.model.response.TokenResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/${api.version}/registrations")
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@Tag(name = "Registration", description = "Registration related resources")
public class RegistrationController {
    private final ElectronicDigitalSignatureSignerUtil electronicDigitalSignatureSignerUtil;
    private final AuthenticationService authenticationService;
    private final SignerUtil signerUtil;

    @PostMapping("/eds-sign")
    @Operation(summary = "Validate EDS", description = "Validating employer by electronic digital signature")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Employer validated and returned with signature information",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema =
                    @Schema(implementation = SignersDto.class))}),
            @ApiResponse(responseCode = "409", description = "Employer is not valid", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public SignersDto getSign(@Valid @RequestBody ElectronicDigitalSignatureXmlDtoRequest electronicDigitalSignatureXmlDtoRequest) throws JsonProcessingException {
        String test = electronicDigitalSignatureSignerUtil.getSign(electronicDigitalSignatureXmlDtoRequest);
        return signerUtil.saveSignerToDto(test);
    }

    @PostMapping
    @Operation(summary = "Create new user", description = "Creating new user and generating new access and refresh" +
            "tokens")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "New user created and returned with new access and refresh tokens",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema =
                    @Schema(implementation = TokenResponse.class))}),
            @ApiResponse(responseCode = "409", description = "User email is already taken", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public ResponseEntity<TokenResponse> register(@Valid @RequestBody RegistrationRequest registrationRequest) {
        return ResponseEntity.status(HttpStatus.CREATED).body(authenticationService.register(registrationRequest, signerUtil.getSignersDto()));
    }
}
