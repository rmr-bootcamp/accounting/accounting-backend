package kz.redmadrobot.api.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import kz.redmadrobot.business.service.impl.ReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@Tag(name = "Report", description = "Report related resources")
@RequestMapping("/${api.version}/reports")
public class ReportController {
    private final ReportService reportService;

    @GetMapping(value = "employee-list")
    public ResponseEntity<InputStreamResource> getEmployeeListByCompanyId(
            @RequestParam(name = "companyId") Long id,
            @RequestParam @Size(min = 1, max = 12) Integer month,
            @RequestParam @Positive Integer year
    ) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Disposition", "attachment; filename=employee-list-report.pdf");
        return ResponseEntity
                .ok()
                .headers(httpHeaders)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(reportService.employeeListByCompanyId(id, month, year)));
    }

    @GetMapping(value = "employee-salary-details")
    public ResponseEntity<InputStreamResource> getEmployeeSalaryDetailsByCompanyIdAndEmployeeId(
            @RequestParam Long companyId,
            @RequestParam Long employeeId,
            @RequestParam @Size(min = 1, max = 12) Integer month,
            @RequestParam @Positive Integer year
    ) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Disposition", "attachment; filename=employee-salary-details-report.pdf");
        return ResponseEntity
                .ok()
                .headers(httpHeaders)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(reportService.getCompanyEmployeeSalaryDetails(companyId, employeeId, month, year)));
    }

}
