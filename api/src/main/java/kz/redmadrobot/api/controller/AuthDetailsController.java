package kz.redmadrobot.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import kz.redmadrobot.api.model.ApiErrorResponse;
import kz.redmadrobot.api.security.service.AuthenticationService;
import kz.redmadrobot.business.model.request.AccessTokenCreateRequest;
import kz.redmadrobot.business.model.request.LoginRequest;
import kz.redmadrobot.business.model.response.AccessTokenResponse;
import kz.redmadrobot.business.model.response.TokenResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RequestMapping("/${api.version}/auth")
@Tag(name = "Authorization details")
@Slf4j
public class AuthDetailsController {
    private final AuthenticationService authenticationService;

    @PostMapping("/login")
    @Operation(summary = "Login", description = "User authorization")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User is authorized and returned with new access and refresh tokens", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = TokenResponse.class))}),
            @ApiResponse(responseCode = "403", description = "User's email or password incorrect", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public TokenResponse login(@Valid @RequestBody LoginRequest loginRequest) {
        return authenticationService.login(loginRequest);
    }

    @PostMapping("/token/refresh")
    @Operation(summary = "Refresh", description = "Refreshing access token by refresh token")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned new access token", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AccessTokenResponse.class))}),
            @ApiResponse(responseCode = "403", description = "JWT validity cannot be asserted and should not be trusted", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public AccessTokenResponse refresh(@Valid @RequestBody AccessTokenCreateRequest accessTokenCreateRequest) {
        return authenticationService.refresh(accessTokenCreateRequest);
    }
}
