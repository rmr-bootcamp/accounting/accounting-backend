package kz.redmadrobot.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import kz.redmadrobot.api.model.ApiErrorResponse;
import kz.redmadrobot.api.security.util.SecurityUtil;
import kz.redmadrobot.business.model.dto.PersonalInfoDto;
import kz.redmadrobot.business.model.request.PersonalInfoUpdateRequest;
import kz.redmadrobot.business.service.impl.PersonalInfoServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/${api.version}/personal-informations")
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@Tag(name = "Personal informations", description = "Personal information related resources")
public class PersonalInfoController {
    private final PersonalInfoServiceImpl personalInfoService;

    @GetMapping
    public PersonalInfoDto getMe() {
        String userEmail = SecurityUtil.getAuthenticatedUser();
        return personalInfoService.get(userEmail);
    }

    @PutMapping
    @Operation(summary = "Update profile information")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Profile information updated", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = PersonalInfoDto.class))}),
            @ApiResponse(responseCode = "409", description = "Profile information is", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorResponse.class))
            })
    })
    public PersonalInfoDto update(@Valid @RequestBody PersonalInfoUpdateRequest personalInfoUpdateRequest) {
        String userEmail = SecurityUtil.getAuthenticatedUser();
        return personalInfoService.update(personalInfoUpdateRequest, userEmail);
    }
}
