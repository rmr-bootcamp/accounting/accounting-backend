package kz.redmadrobot.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import org.springframework.http.HttpStatus;

@Builder
@Schema(description = "Provides more specific information about error occurred. " +
        "Messages can be localized to show the user the reason of operation failure")
public record ApiUnauthorizedResponse(@Schema(example = "403") HttpStatus httpStatus,
                               @Schema(example = "User is unauthorized") String message) {
}
