package kz.redmadrobot.api.component;

import kz.redmadrobot.api.model.ApiErrorResponse;
import kz.redmadrobot.api.model.ApiResponse;
import kz.redmadrobot.business.exception.employee.EmployeeMinimumWageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class EmployeeControllerAdvice {
    @ExceptionHandler(EmployeeMinimumWageException.class)
    private ResponseEntity<ApiErrorResponse> attendanceDateException(EmployeeMinimumWageException exception) {
        return ApiResponse.buildErrorResponse(HttpStatus.CONFLICT, exception.getMessage());
    }
}
