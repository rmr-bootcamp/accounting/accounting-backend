package kz.redmadrobot.api.component;

import kz.redmadrobot.api.model.ApiErrorResponse;
import kz.redmadrobot.api.model.ApiResponse;
import kz.redmadrobot.business.exception.attendancehour.HourlyAttendanceExceedingLimitByHoursException;
import kz.redmadrobot.business.exception.attendancehour.HourlyAttendanceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HourlyAttendanceControllerAdvice {
    @ExceptionHandler(HourlyAttendanceExceedingLimitByHoursException.class)
    private ResponseEntity<ApiErrorResponse> hourlyAttendanceExceedingLimitByHoursException(
            HourlyAttendanceExceedingLimitByHoursException exception) {
        return ApiResponse.buildErrorResponse(HttpStatus.CONFLICT, exception.getMessage());
    }

    @ExceptionHandler(HourlyAttendanceNotFoundException.class)
    private ResponseEntity<ApiErrorResponse> hourlyAttendanceNotFoundException(HourlyAttendanceNotFoundException exception) {
        return ApiResponse.buildErrorResponse(HttpStatus.NOT_FOUND, exception.getMessage());
    }
}
