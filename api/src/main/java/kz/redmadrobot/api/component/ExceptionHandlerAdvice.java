package kz.redmadrobot.api.component;

import jakarta.persistence.EntityNotFoundException;
import kz.redmadrobot.api.model.ApiErrorResponse;
import kz.redmadrobot.api.model.ApiResponse;
import kz.redmadrobot.business.exception.global.EmailExistsException;
import kz.redmadrobot.business.exception.global.EntityNameExistsException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class ExceptionHandlerAdvice {
    @ExceptionHandler(HttpMessageNotReadableException.class)
    private ResponseEntity<ApiErrorResponse> httpMessageNotReadableHandler(HttpMessageNotReadableException exception) {
        return ApiResponse.buildErrorResponse(HttpStatus.BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler(EntityNotFoundException.class)
    private ResponseEntity<ApiErrorResponse> entityNotFoundHandler(EntityNotFoundException exception) {
        return ApiResponse.buildErrorResponse(HttpStatus.NOT_FOUND, exception.getMessage());
    }

    @ExceptionHandler(EntityNameExistsException.class)
    private ResponseEntity<ApiErrorResponse> entityNameExistsHandler(EntityNameExistsException exception) {
        return ApiResponse.buildErrorResponse(HttpStatus.CONFLICT, exception.getMessage());
    }

    @ExceptionHandler(EmailExistsException.class)
    private ResponseEntity<ApiErrorResponse> emailExistsException(EmailExistsException exception) {
        return ApiResponse.buildErrorResponse(HttpStatus.CONFLICT, exception.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiErrorResponse> handleValidationExceptions(MethodArgumentNotValidException ex) {
        List<String> errors = new ArrayList<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String errorMessage = error.getDefaultMessage();
            errors.add(errorMessage);
        });
        return ApiResponse.buildErrorResponse(HttpStatus.BAD_REQUEST, errors.toString().replaceAll("^\\[|\\]$", ""));
    }
}
