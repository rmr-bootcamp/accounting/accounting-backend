package kz.redmadrobot.api.component;

import kz.redmadrobot.api.model.ApiErrorResponse;
import kz.redmadrobot.api.model.ApiResponse;
import kz.redmadrobot.business.exception.debitinghistory.DebitingHistoryException;
import kz.redmadrobot.business.exception.report.ReportException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ReportControllerAdvice {
    @ExceptionHandler(DebitingHistoryException.class)
    private ResponseEntity<ApiErrorResponse> debitingHistoryException(DebitingHistoryException exception) {
        return ApiResponse.buildErrorResponse(HttpStatus.BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler(ReportException.class)
    private ResponseEntity<ApiErrorResponse> reportException(ReportException exception) {
        return ApiResponse.buildErrorResponse(HttpStatus.BAD_REQUEST, exception.getMessage());
    }
}
