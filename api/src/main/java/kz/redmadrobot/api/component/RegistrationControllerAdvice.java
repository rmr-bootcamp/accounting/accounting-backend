package kz.redmadrobot.api.component;

import kz.redmadrobot.api.model.ApiErrorResponse;
import kz.redmadrobot.api.model.ApiResponse;
import kz.redmadrobot.business.exception.registration.UserIsNotValidException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RegistrationControllerAdvice {
    @ExceptionHandler(UserIsNotValidException.class)
    private ResponseEntity<ApiErrorResponse> userIsNotValidException(
            UserIsNotValidException exception) {
        return ApiResponse.buildErrorResponse(HttpStatus.CONFLICT, exception.getMessage());
    }
}
