package kz.redmadrobot.api.component;

import kz.redmadrobot.api.model.ApiErrorResponse;
import kz.redmadrobot.api.model.ApiResponse;
import kz.redmadrobot.business.exception.attendance.AttendanceDateException;
import kz.redmadrobot.business.exception.attendance.AttendanceIsExistsException;
import kz.redmadrobot.business.exception.attendance.AttendanceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AttendanceControllerAdvice {
    @ExceptionHandler(AttendanceDateException.class)
    private ResponseEntity<ApiErrorResponse> attendanceDateException(AttendanceDateException exception) {
        return ApiResponse.buildErrorResponse(HttpStatus.CONFLICT, exception.getMessage());
    }

    @ExceptionHandler(AttendanceNotFoundException.class)
    private ResponseEntity<ApiErrorResponse> attendanceNotFoundException(AttendanceNotFoundException exception) {
        return ApiResponse.buildErrorResponse(HttpStatus.NOT_FOUND, exception.getMessage());
    }

    @ExceptionHandler(AttendanceIsExistsException.class)
    private ResponseEntity<ApiErrorResponse> attendanceIsExistsException(AttendanceIsExistsException exception) {
        return ApiResponse.buildErrorResponse(HttpStatus.CONFLICT, exception.getMessage());
    }
}
