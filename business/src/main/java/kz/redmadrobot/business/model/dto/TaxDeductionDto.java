package kz.redmadrobot.business.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema
public record TaxDeductionDto(@Schema(example = "1") Long id,
                              //TODO добавить налог
                              @Schema(example = "12000") Long amount) {
}