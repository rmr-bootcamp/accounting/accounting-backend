package kz.redmadrobot.business.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@Schema(description = "For verification electronic digital signature signer")
public class SignerCreateRequest {
    @NotNull(message = "Xml must not be null or empty")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "MIINKwIBAzCCDOUGCS...")
    private String key;

    @NotNull(message = "Password must not be null or empty")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "Qwerty12")
    private String password;

    @NotNull(message = "Key alias must not be null or empty")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED)
    private String keyAlias;
}