package kz.redmadrobot.business.model.dto;

import java.util.Date;

public record RevocationDto(
        boolean revoked,
        String by,

        Date revocationTime,
        String reason) {
}
