package kz.redmadrobot.business.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import kz.redmadrobot.business.model.enums.LegalForm;

@Schema(description = "Object with full company data")
public record CompanyDto(@Schema(example = "1") Long id,
                         @Schema(example = "Alumax Industries") String companyName,
                         @Schema(example = "info@alumax.uz") String corporateEmail,
                         @Schema(example = "LLP") LegalForm legalForm) {
}
