package kz.redmadrobot.business.model.enums.sort;

public enum CompanySortBy {
    ID("id"),
    COMPANY_NAME("companyName"),
    CORPORATE_EMAIL("corporateEmail"),
    LEGAL_FORM("LEGAL_FORM");

    private final String attribute;

    CompanySortBy(String attribute) {
        this.attribute = attribute;
    }

    public String getAttribute() {
        return attribute;
    }
}
