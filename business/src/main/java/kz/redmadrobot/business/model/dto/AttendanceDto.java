package kz.redmadrobot.business.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import kz.redmadrobot.business.model.enums.AttendanceType;

@Schema(description = "Object with full attendance data")
public record AttendanceDto(@Schema(example = "1") Long id,
                            @Schema(example = "absent") AttendanceType type,
                            @Schema(example = "2023-03-20") String reportedStartDate,
                            @Schema(example = "2023-03-25") String reportedEndDate) {
}