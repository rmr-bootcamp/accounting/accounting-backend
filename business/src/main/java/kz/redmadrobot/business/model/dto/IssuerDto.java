package kz.redmadrobot.business.model.dto;

public record IssuerDto(String commonName,
                        String lastName,
                        String surName,
                        String email,
                        String organization,
                        String gender,
                        String iin,
                        String bin,
                        String country,
                        String locality,
                        String state) {
}
