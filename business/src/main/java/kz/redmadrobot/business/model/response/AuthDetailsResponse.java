package kz.redmadrobot.business.model.response;

import lombok.Builder;

@Builder
public record AuthDetailsResponse(
        String commonName,
        String lastName,
        String surName,
        String organization,
        String gender,
        String iin,
        String bin,
        String country,
        String locality,
        String state
) {
}
