package kz.redmadrobot.business.model.dto;

public record SubjectDto(
        String commonName,
        String lastName,
        String surName,
        String email,
        String organization,
        String gender,
        String iin,
        String bin,
        String country,
        String locality,
        String state
) { }
