package kz.redmadrobot.business.model.dto;

import java.util.List;

public record SignersDto(
        int status,
        String message,
        boolean valid,
        List<SignerDto> signers
) {
}
