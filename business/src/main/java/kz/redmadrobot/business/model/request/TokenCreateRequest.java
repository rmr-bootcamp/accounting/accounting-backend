package kz.redmadrobot.business.model.request;

import kz.redmadrobot.business.model.dto.AuthDetailsDto;
import lombok.Data;

@Data
public class TokenCreateRequest {
    private String token;

    private AuthDetailsDto authDetailsDto;
}
