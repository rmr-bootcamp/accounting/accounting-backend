package kz.redmadrobot.business.model.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum HourlyAttendanceType {
    OVERTIME("overtime"),
    LATE_FINE("late fine");

    private final String attribute;

    HourlyAttendanceType(String attribute) {
        this.attribute = attribute;
    }

    @JsonValue
    public String getAttribute() {
        return attribute;
    }
}
