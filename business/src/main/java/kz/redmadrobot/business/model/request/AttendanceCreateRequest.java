package kz.redmadrobot.business.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import kz.redmadrobot.business.model.enums.AttendanceType;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@Schema(description = "For creating new attendance")
public class AttendanceCreateRequest {
    @NotNull(message = "Type must not be empty")
    @Schema(example = "absent")
    private AttendanceType type;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @NotNull(message = "Reported start date must not be empty")
    @Schema(example = "2023-03-20")
    private LocalDate reportedStartDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @NotNull(message = "Reported end date must not be empty")
    @Schema(example = "2023-03-25")
    private LocalDate reportedEndDate;
}
