package kz.redmadrobot.business.model.dto;

public record AuthDetailsDto(String email,
                             String password) {
}
