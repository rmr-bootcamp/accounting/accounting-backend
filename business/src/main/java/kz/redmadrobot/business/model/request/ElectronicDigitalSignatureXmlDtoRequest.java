package kz.redmadrobot.business.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Data
@Schema(description = "For verification electronic digital signature")
public class ElectronicDigitalSignatureXmlDtoRequest {
    @NotNull(message = "Xml must not be null or empty")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "<?xml version=\"1.0\" encoding=\"utf-8\"?><a><b>test</b></a>")
    private String xml;

    @NotNull(message = "Signers must not be null")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED)
    private List<SignerCreateRequest> signers;

    @NotNull(message = "Clear signatures must not be null")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED)
    private boolean clearSignatures;

    @NotNull(message = "Trim xml must not be null")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED)
    private boolean trimXml = false;
}
