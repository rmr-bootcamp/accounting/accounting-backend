package kz.redmadrobot.business.model.enums;

public enum LegalForm {
    IIE,
    LLP
}
