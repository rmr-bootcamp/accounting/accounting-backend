package kz.redmadrobot.business.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

@Schema(description = "Object with full attendance data")
public record AttendancesListDto(List<AttendanceDto> attendanceList) {
}
