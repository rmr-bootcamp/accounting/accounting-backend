package kz.redmadrobot.business.model.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import kz.redmadrobot.business.model.enums.LegalForm;
import lombok.Data;

@Data
@Schema(description = "For creating new company")
public class CompanyCreateRequest {

    @NotBlank
    @NotEmpty(message = "Company name should not be null or empty")
    @Schema(example = "Alumax Industries")
    private String companyName;

    @Email(message = "Corporate email must be in email format")
    @NotEmpty(message = "Corporate email should not be null or empty")
    @Schema(example = "info@alumax.uz")
    private String corporateEmail;

    @NotNull(message = "Legal form must not be empty")
    @Schema(allowableValues = {"IE", "LLP"}, example = "LLP")
    private LegalForm legalForm;

    @JsonIgnore
    private String userEmail;
}
