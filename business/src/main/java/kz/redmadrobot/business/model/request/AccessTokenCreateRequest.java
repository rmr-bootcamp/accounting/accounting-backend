package kz.redmadrobot.business.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Schema(description = "For creating new access token by refresh token")
public class AccessTokenCreateRequest {
    @NotEmpty(message = "Refresh token must not be null or empty")
    @Schema(example = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhbWlya2VuZXNiYXkzQGdtYWlsLmNvbSIsImlhdCI6MTY4MDcyMDUwNSwiZXhwIjoxNjk2NDk5MzA1fQ.OvgDR80HqXTDHLjaJuaf-aNzWr4kHWvmzYDyOLGxb0Y")
    private String refreshToken;
}
