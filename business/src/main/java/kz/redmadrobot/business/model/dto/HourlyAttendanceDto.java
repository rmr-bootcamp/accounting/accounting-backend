package kz.redmadrobot.business.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import kz.redmadrobot.business.model.enums.HourlyAttendanceType;

@Schema(description = "Object with full hourly attendance data")
public record HourlyAttendanceDto(@Schema(example = "1") Long id,
                                  @Schema(example = "overtime") HourlyAttendanceType type,
                                  @Schema(example = "2023-04-07") String reportedDate,
                                  @Schema(example = "4") String hours,
                                  @Schema(example = "30") String minutes
) {
}
