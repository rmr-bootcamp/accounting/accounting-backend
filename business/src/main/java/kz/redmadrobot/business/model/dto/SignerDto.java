package kz.redmadrobot.business.model.dto;

import java.util.Date;
import java.util.List;

public record SignerDto(
        Integer status,
        String message,
        boolean valid,
        List<RevocationDto> revocations,
        Date notBefore,

        Date notAfter,
        String keyUsage,
        String serialNumber,
        String signAlg,
        List<String> keyUser,
        String publicKey,
        String signature,
        SubjectDto subject,
        IssuerDto issuer) {
}
