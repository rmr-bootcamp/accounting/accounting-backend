package kz.redmadrobot.business.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import kz.redmadrobot.business.model.enums.HourlyAttendanceType;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@Schema(description = "For creating hourly attendance to employee")
public class HourlyAttendanceCreateRequest {
    @NotNull(message = "Hourly attendance type must not be empty")
    @Schema(example = "overtime")
    private HourlyAttendanceType type;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @NotNull(message = "Hourly attendance reported date must not be empty")
    @Schema(example = "2023-04-07")
    private LocalDate reportedDate;

    @Max(value = 23, message = "Hourly attendance hours must be maximum 23")
    @Min(value = 0, message = "Hourly attendance hours must be minimum 0")
    @NotNull(message = "Hourly attendance hours must not be empty")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "1")
    private Integer hours;

    @Max(value = 59, message = "Hourly attendance minutes must be maximum 59")
    @Min(value = 0, message = "Hourly attendance minutes must be minimum 0")
    @NotNull(message = "Hourly attendance minutes must not be empty")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "20")
    private Integer minutes;
}
