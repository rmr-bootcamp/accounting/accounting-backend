package kz.redmadrobot.business.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import kz.redmadrobot.dao.enums.ResidentStatus;
import kz.redmadrobot.dao.enums.SocialStatus;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Schema(description = "Object with full employee data")
public record EmployeeDto(
    @Schema(example = "1") Long id,
    @Schema(example = "2023-03-06") LocalDate hiredAt,
    @Schema(example = "John") String firstName,
    @Schema(example = "Doe") String lastName,
    @Schema(example = "son of Willian") String patronymic,
    @Schema(example = "john_doe@gmail.com") String email,
    @Schema(example = "450000") BigDecimal grossSalary,
    @Schema(example = "RESIDENT") ResidentStatus residentStatus,
    @Schema(example = "[\"PENSIONER\", \"HANDICAPPED\"]") Set<SocialStatus> socialStatuses,
    @Schema CompanyDto company
) {

}