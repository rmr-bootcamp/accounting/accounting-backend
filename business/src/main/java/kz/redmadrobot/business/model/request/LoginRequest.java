package kz.redmadrobot.business.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
@Schema(description = "For authorization user")
public class LoginRequest {
    @Email(message = "Personal email must be in email format")
    @NotEmpty(message = "Username must not be null or empty")
    @Schema(example = "salarytimeuser@gmail.com")
    private String email;

    @NotEmpty(message = "Username must not be null or empty")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=_])(?=\\S+$).{8,}$", message = "Password must have" +
            "1) At least 8 chars, " +
            "2) Contains at least one digit, " +
            "3) Contains at least one lower alpha char and one upper alpha char, " +
            "4) Contains at least one char within a set of special chars (@#%$^ etc.), " +
            "5) Does not contain space, tab, etc."
    )
    @Schema(example = "Qwerty_12")
    private String password;
}