package kz.redmadrobot.business.model.enums.sort;

public enum SocialStatusSortBy {

    ID("id"),
    STATUS_NAME("statusName");

    private final String attribute;

    SocialStatusSortBy(String attribute) {
        this.attribute = attribute;
    }

    public String getAttribute() {
        return attribute;
    }
}
