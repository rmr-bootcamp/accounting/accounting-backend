package kz.redmadrobot.business.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.constraints.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;
import kz.redmadrobot.dao.enums.ResidentStatus;
import kz.redmadrobot.dao.enums.SocialStatus;
import lombok.Data;

@Data
@Schema(description = "For creating only user. It is assumed, that company and social statuses are already defined.")
public class EmployeeCreateRequest {

    @NotNull(message = "hiredAt must be not null")
    @Schema(requiredMode = RequiredMode.REQUIRED, example = "2023-03-05")
    private LocalDate hiredAt;

    @NotBlank(message = "Employees' first name must not be null or empty")
    @Schema(requiredMode = RequiredMode.REQUIRED, example = "John")
    private String firstName;

    @NotBlank(message = "Employees' last name must not be null or empty")
    @Schema(requiredMode = RequiredMode.REQUIRED, example = "Doe")
    private String lastName;

    @NotBlank(message = "Employees' patronymic must not be null or empty")
    @Schema(requiredMode = RequiredMode.NOT_REQUIRED, example = "son of William")
    private String patronymic;

    @Email(message = "Employee's email must be in email format")
    @Schema(requiredMode = RequiredMode.REQUIRED, example = "john_doe@gmail.com")
    private String email;

    @Min(70000)
    @Schema(requiredMode = RequiredMode.REQUIRED, minimum = "70000",
            description = "From January 1st of 2023 minimum wage is 70000 tenge", example = "450000")
    private BigDecimal grossSalary;

    @NotNull(message = "Employees' resident status must not be null")
    @Schema(requiredMode = RequiredMode.REQUIRED, example = "RESIDENT")
    private ResidentStatus residentStatus;

    @NotNull(message = "socialStatuses must not be null")
    @Schema(requiredMode = RequiredMode.REQUIRED, example = "[\"PENSIONER\", \"HANDICAPPED\" ]")
    private Set<SocialStatus> socialStatuses;

    @NotNull(message = "Employees' company id must not be null or empty")
    @Positive
    @Schema(requiredMode = RequiredMode.REQUIRED, example = "1")
    private Long companyId;
}
