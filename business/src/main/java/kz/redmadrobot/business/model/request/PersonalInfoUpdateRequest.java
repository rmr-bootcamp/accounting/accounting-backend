package kz.redmadrobot.business.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Schema(description = "For updating personal information")
public class PersonalInfoUpdateRequest {
    @NotEmpty(message = "Common name must not be null or empty")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "Герман Шульц")
    String commonName;

    @NotEmpty(message = "Last name must not be null or empty")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "Герман")
    String lastName;

    @NotEmpty(message = "Surname must not be null or empty")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "Шульц")
    String surName;

    @NotEmpty(message = "Gender must not be null or empty")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "MALE")
    String gender;

    @NotEmpty(message = "Country must not be null or empty")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "KZ")
    String country;

    @NotEmpty(message = "Locality must not be null or empty")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "ASTANA")
    String locality;

    @NotEmpty(message = "State must not be null or empty")
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, example = "ASTANA")
    String state;
}
