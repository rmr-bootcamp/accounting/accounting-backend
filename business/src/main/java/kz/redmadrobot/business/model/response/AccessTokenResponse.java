package kz.redmadrobot.business.model.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;

@Builder
@Schema(description = "New access token response object")
public record AccessTokenResponse(
        @Schema(example = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzYWxhcnl0aW1ldXNlckBnbWFpbC5jb20iLCJpYXQiOjE2ODA4MDY0OTIsImV4cCI6MTY4MDgwNzY5Mn0.x415LZJnL2Kl4BTG-z8MmWKCkVK35Cz_vu5UVp6Aovc")
        String accessToken
) {
}
