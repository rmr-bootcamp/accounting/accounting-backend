package kz.redmadrobot.business.model.enums.sort;

public enum AttendanceSortBy {
    ID("id"),
    TYPE("type"),
    START_DATE("startDate"),
    END_DATE("endDate");

    private final String attribute;

    AttendanceSortBy(String attribute) {
        this.attribute = attribute;
    }

    public String getAttribute() {
        return attribute;
    }
}
