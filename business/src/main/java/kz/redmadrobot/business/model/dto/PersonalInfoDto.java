package kz.redmadrobot.business.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Object with full personal info")
public record PersonalInfoDto(
        @Schema(example = "Герман Шульц")
        String commonName,
        @Schema(example = "Герман")
        String lastName,
        @Schema(example = "Шульц")
        String surname,
        @Schema(example = "ТОВАРИЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ Infotec")
        String organization,
        @Schema(example = "MALE")
        String gender,
        @Schema(example = "7716855198")
        String iin,
        @Schema(example = "7716855198")
        String bin,
        @Schema(example = "KZ")
        String country,
        @Schema(example = "ASTANA")
        String locality,
        @Schema(example = "ASTANA")
        String state
) {
}
