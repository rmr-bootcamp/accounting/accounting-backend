package kz.redmadrobot.business.model.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum AttendanceType {
    ABSENT("absent"),
    BUSINESS_TRIP("business trip"),
    SICK_LEAVE("sick leave"),
    VACATION("vacation"),
    UNPAID_VACATION("unpaid vacation");

    private final String attribute;

    AttendanceType(String attribute) {
        this.attribute = attribute;
    }

    @JsonValue
    public String getAttribute() {
        return attribute;
    }
}
