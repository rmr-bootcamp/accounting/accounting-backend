package kz.redmadrobot.business.model.enums.sort;

public enum EmployeeSortBy {
    ID("id"),
    FIRST_NAME("firstName"),
    LAST_NAME("lastName"),
    PATRONYMIC("firstName"),
    EMAIL("email"),
    GROSS_SALARY("grossSalary"),
    RESIDENT_STATUS("residentStatus");

    private final String attribute;

    EmployeeSortBy(String attribute) {
        this.attribute = attribute;
    }

    public String getAttribute() {
        return attribute;
    }
}
