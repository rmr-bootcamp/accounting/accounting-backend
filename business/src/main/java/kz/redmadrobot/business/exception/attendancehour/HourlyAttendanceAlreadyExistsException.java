package kz.redmadrobot.business.exception.attendancehour;

public class HourlyAttendanceAlreadyExistsException extends RuntimeException {
    public HourlyAttendanceAlreadyExistsException(String message) {
        super(message);
    }
}
