package kz.redmadrobot.business.exception.report;

public class ReportException extends RuntimeException {
    public ReportException(String message) {
        super(message);
    }
}
