package kz.redmadrobot.business.exception.employee;

public class EmployeeMinimumWageException extends RuntimeException {
    public EmployeeMinimumWageException(String message) {
        super(message);
    }
}
