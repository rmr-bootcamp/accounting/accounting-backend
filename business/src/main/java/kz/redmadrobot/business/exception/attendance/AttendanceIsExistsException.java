package kz.redmadrobot.business.exception.attendance;

public class AttendanceIsExistsException extends RuntimeException {
    public AttendanceIsExistsException(String message) {
        super(message);
    }
}
