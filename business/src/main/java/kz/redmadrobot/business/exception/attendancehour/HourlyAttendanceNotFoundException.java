package kz.redmadrobot.business.exception.attendancehour;

public class HourlyAttendanceNotFoundException extends RuntimeException {
    public HourlyAttendanceNotFoundException(String message) {
        super(message);
    }
}
