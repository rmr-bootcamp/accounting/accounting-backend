package kz.redmadrobot.business.exception.attendance;

public class AttendanceDateException extends RuntimeException {
    public AttendanceDateException(String message) {
        super(message);
    }
}
