package kz.redmadrobot.business.exception.attendancehour;

public class HourlyAttendanceExceedingLimitByHoursException extends RuntimeException {
    public HourlyAttendanceExceedingLimitByHoursException(String message) {
        super(message);
    }
}
