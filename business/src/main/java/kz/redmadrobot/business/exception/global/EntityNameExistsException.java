package kz.redmadrobot.business.exception.global;

public class EntityNameExistsException extends RuntimeException {
    public EntityNameExistsException(String message) {
        super(message);
    }
}