package kz.redmadrobot.business.exception.debitinghistory;

public class DebitingHistoryException extends RuntimeException {
    public DebitingHistoryException(String message) {
        super(message);
    }
}
