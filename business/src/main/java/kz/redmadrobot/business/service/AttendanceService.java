package kz.redmadrobot.business.service;

import kz.redmadrobot.business.model.dto.AttendanceDto;
import kz.redmadrobot.business.model.request.AttendanceCreateRequest;

import java.util.List;

public interface AttendanceService {
    AttendanceDto create(Long employeeId, AttendanceCreateRequest attendanceCreateRequest);

    AttendanceDto getOne(Long employeeId, Long attendanceId);

    List<AttendanceDto> getAttendanceByDate(String reportedStartDate, String reportedEndDate, Long employeeId);

    AttendanceDto update(Long employeeId, Long attendanceId, AttendanceCreateRequest attendanceCreateRequest);

    void deleteById(Long employeeId, Long attendanceId);
}
