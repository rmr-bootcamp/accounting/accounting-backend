package kz.redmadrobot.business.service.impl;

import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import kz.redmadrobot.business.exception.attendance.AttendanceDateException;
import kz.redmadrobot.business.exception.attendancehour.HourlyAttendanceAlreadyExistsException;
import kz.redmadrobot.business.exception.attendancehour.HourlyAttendanceNotFoundException;
import kz.redmadrobot.business.mapper.dto.HourlyAttendanceMapper;
import kz.redmadrobot.business.mapper.request.HourlyAttendanceCreateMapper;
import kz.redmadrobot.business.model.dto.HourlyAttendanceDto;
import kz.redmadrobot.business.model.enums.HourlyAttendanceType;
import kz.redmadrobot.business.model.request.HourlyAttendanceCreateRequest;
import kz.redmadrobot.business.utils.SalaryCalculator;
import kz.redmadrobot.dao.entity.Attendance;
import kz.redmadrobot.dao.entity.Employee;
import kz.redmadrobot.dao.entity.HourlyAttendance;
import kz.redmadrobot.dao.repository.AttendanceRepository;
import kz.redmadrobot.dao.repository.EmployeeRepository;
import kz.redmadrobot.dao.repository.HourlyAttendanceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class HourlyAttendanceServiceImpl {
    private final HourlyAttendanceCreateMapper hourlyAttendanceCreateMapper;
    private final HourlyAttendanceMapper hourlyAttendanceMapper;
    private final HourlyAttendanceRepository hourlyAttendanceRepository;
    private final EmployeeRepository employeeRepository;
    private final SalaryCalculator salaryCalculator;
    private final AttendanceRepository attendanceRepository;

    public HourlyAttendanceDto create(Long employeeId, HourlyAttendanceCreateRequest hourlyAttendanceCreateRequest) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Employee with id '%s' not found!", employeeId)));

        HourlyAttendanceType hourlyAttendanceType = hourlyAttendanceCreateRequest.getType();
        LocalDate hourlyAttendanceReportedDate = hourlyAttendanceCreateRequest.getReportedDate();
        Integer hourlyAttendanceHours = hourlyAttendanceCreateRequest.getHours();
        Integer hourlyAttendanceMinutes = hourlyAttendanceCreateRequest.getMinutes();

        log.info("Creating HourlyAttendance with type='{}', reported date = '{}', hourlyAttendanceHours = '{}', hourlyAttendanceMinutes = '{}'",
                hourlyAttendanceType, hourlyAttendanceReportedDate, hourlyAttendanceHours, hourlyAttendanceMinutes);

        List<Attendance> attendanceList = attendanceRepository.findAttendanceByDateAndEmployeeId(hourlyAttendanceReportedDate, hourlyAttendanceReportedDate, employee.getId());
        if (!attendanceList.isEmpty()) {
            throw new AttendanceDateException(String.format("Attendance with this date '%s' is already exists!",
                    hourlyAttendanceReportedDate));
        }

        Optional<HourlyAttendance> optionalHourlyAttendance = hourlyAttendanceRepository.findHourlyAttendanceByDateAndEmployeeId(hourlyAttendanceReportedDate, employeeId);
        if (optionalHourlyAttendance.isPresent()) {
            throw new HourlyAttendanceAlreadyExistsException(String.format("HourlyAttendance reported date '%s' is already exists!",
                    hourlyAttendanceReportedDate));
        }
        HourlyAttendance hourlyAttendance = hourlyAttendanceCreateMapper.toEntity(hourlyAttendanceCreateRequest);
        return getHourlyAttendanceDto(employee, hourlyAttendanceType, hourlyAttendanceReportedDate, hourlyAttendanceHours, hourlyAttendanceMinutes, hourlyAttendance);
    }

    public HourlyAttendanceDto getOne(Long employeeId, Long hourlyAttendanceId) {
        log.info("Searching for HourlyAttendance with id='{}' and Employee with id = '{}'", hourlyAttendanceId, employeeId);

        HourlyAttendance hourlyAttendance = hourlyAttendanceRepository.findById(hourlyAttendanceId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("HourlyAttendance with id '%s' not found!", hourlyAttendanceId))
                );

        if (!hourlyAttendance.getEmployee().getId().equals(employeeId)) {
            throw new HourlyAttendanceNotFoundException(String.format("HourlyAttendance with id='%s' does not belong to the employee id='%s'",
                    hourlyAttendanceId, employeeId));
        }

        return hourlyAttendanceMapper.toDto(hourlyAttendance);
    }

    public HourlyAttendanceDto getByDate(Long employeeId, String hourlyAttendanceReportedDate) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Employee with id '%s' not found!", employeeId)));

        LocalDate reportedDate = LocalDate.parse(hourlyAttendanceReportedDate, DateTimeFormatter.ISO_LOCAL_DATE);

        HourlyAttendance hourlyAttendance = hourlyAttendanceRepository.findHourlyAttendanceByDateAndEmployeeId(reportedDate,
                employee.getId()).orElseThrow(() -> new EntityNotFoundException(
                String.format("HourlyAttendance with reported date '%s' not found!", reportedDate)));

        return hourlyAttendanceMapper.toDto(hourlyAttendance);
    }

    public HourlyAttendanceDto update(Long employeeId, HourlyAttendanceCreateRequest hourlyAttendanceCreateRequest) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Employee with id '%s' not found!", employeeId)));

        HourlyAttendanceType hourlyAttendanceType = hourlyAttendanceCreateRequest.getType();
        LocalDate hourlyAttendanceReportedDate = hourlyAttendanceCreateRequest.getReportedDate();
        Integer hourlyAttendanceHours = hourlyAttendanceCreateRequest.getHours();
        Integer hourlyAttendanceMinutes = hourlyAttendanceCreateRequest.getMinutes();

        log.info("Updating HourlyAttendance with type='{}', reported date = '{}', hourlyAttendanceHours = '{}', hourlyAttendanceMinutes = '{}'",
                hourlyAttendanceType, hourlyAttendanceReportedDate, hourlyAttendanceHours, hourlyAttendanceMinutes);

        List<Attendance> attendanceList = attendanceRepository.findAttendanceByDateAndEmployeeId(hourlyAttendanceReportedDate, hourlyAttendanceReportedDate, employee.getId());
        if (!attendanceList.isEmpty()) {
            throw new AttendanceDateException(String.format("Attendance with this date '%s' is already exists!",
                    hourlyAttendanceReportedDate));
        }

        Optional<HourlyAttendance> optionalHourlyAttendance = hourlyAttendanceRepository.findHourlyAttendanceByDateAndEmployeeId(hourlyAttendanceReportedDate, employeeId);
        if (optionalHourlyAttendance.isPresent()) {
            HourlyAttendance hourlyAttendance = optionalHourlyAttendance.get();
            return getHourlyAttendanceDto(employee, hourlyAttendanceType, hourlyAttendanceReportedDate, hourlyAttendanceHours, hourlyAttendanceMinutes, hourlyAttendance);
        }
        throw new HourlyAttendanceNotFoundException(String.format("HourlyAttendance with this date '%s' is not found!", hourlyAttendanceReportedDate));
    }

    public String deleteByEmployeeId(Long employeeId) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Employee with id '%s' not found!", employeeId)));

        hourlyAttendanceRepository.deleteByEmployeeId(employee.getId());
        return "Successfully deleted all HourlyAttendances";
    }

    public String deleteByDate(String hourlyAttendanceReportedDate, Long employeeId) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Employee with id '%s' not found!", employeeId)));

        LocalDate reportedDate = LocalDate.parse(hourlyAttendanceReportedDate, DateTimeFormatter.ISO_LOCAL_DATE);

        HourlyAttendance hourlyAttendance = hourlyAttendanceRepository.findHourlyAttendanceByDateAndEmployeeId(reportedDate,
                employee.getId()).orElseThrow(() -> new EntityNotFoundException(
                String.format("HourlyAttendance with reported date '%s' not found!", reportedDate)));

        hourlyAttendanceRepository.deleteHourlyAttendanceByEmployeeId(hourlyAttendance.getReportedDate(), employee.getId());
        return String.format("Successfully deleted HourlyAttendance with date '%s'", hourlyAttendanceReportedDate);
    }

    private HourlyAttendanceDto getHourlyAttendanceDto(Employee employee, HourlyAttendanceType hourlyAttendanceType,
                                                       LocalDate hourlyAttendanceReportedDate, Integer hourlyAttendanceHours,
                                                       Integer hourlyAttendanceMinutes, HourlyAttendance hourlyAttendance) {
        hourlyAttendance.setEmployee(employee);

        BigDecimal grossSalary = hourlyAttendance.getEmployee().getGrossSalary();
        BigDecimal convertedHours = salaryCalculator.convertHourlyAttendanceTimeToHours(hourlyAttendanceHours, hourlyAttendanceMinutes);
        BigDecimal oneHourOfEmployeeSalary = salaryCalculator.calculateSalaryForOneHour(hourlyAttendanceReportedDate, grossSalary);
        BigDecimal amountSalaryByHourlyAttendance = salaryCalculator.calculateAmountOfSalaryByHourlyAttendanceType(hourlyAttendanceType,
                grossSalary, oneHourOfEmployeeSalary, convertedHours);

        hourlyAttendance.setAmount(amountSalaryByHourlyAttendance);
        hourlyAttendance.setReportedDate(hourlyAttendanceReportedDate);
        hourlyAttendance.setHours(hourlyAttendanceHours);
        hourlyAttendance.setMinutes(hourlyAttendanceMinutes);
        hourlyAttendanceRepository.save(hourlyAttendance);
        return hourlyAttendanceMapper.toDto(hourlyAttendance);
    }
}
