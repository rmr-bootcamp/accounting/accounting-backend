package kz.redmadrobot.business.service.tax_calculation.employee;

import kz.redmadrobot.dao.entity.TaxDeduction;
import kz.redmadrobot.dao.enums.Tax;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class SalaryEmployeeNonResidentTaxDeductionCalculator implements SalaryEmployeeTaxDeductionCalculator {


    @Override
    public TaxDeduction calculateVOSMS(BigDecimal salary) {
        TaxDeduction taxDeduction = new TaxDeduction();
        taxDeduction.setTax(Tax.VOSMS);
        taxDeduction.setAmount(0L);
        return taxDeduction;
    }

    @Override
    public TaxDeduction calculateOPV(BigDecimal salary) {
        TaxDeduction taxDeduction = new TaxDeduction();
        taxDeduction.setTax(Tax.OPV);
        taxDeduction.setAmount(0L);
        return taxDeduction;
    }

    // TODO неиспользуемые vosmsAmount  opvAmount
    @Override
    public TaxDeduction calculateIPN(BigDecimal salary, Long vosmsAmount, Long opvAmount) {
        TaxDeduction taxDeduction = new TaxDeduction();
        Tax ipn = Tax.IPN;
        taxDeduction.setTax(ipn);
        long amount = salary
                .multiply(ipn.getInterestRate())
                .longValue();
        taxDeduction.setAmount(amount);
        return taxDeduction;
    }
}
