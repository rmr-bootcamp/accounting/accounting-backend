package kz.redmadrobot.business.service;

import java.util.Set;
import kz.redmadrobot.business.model.dto.CompanyDto;
import kz.redmadrobot.business.model.enums.LegalForm;
import kz.redmadrobot.business.model.enums.sort.CompanySortBy;
import kz.redmadrobot.business.model.enums.sort.SortType;
import kz.redmadrobot.business.model.request.CompanyCreateRequest;

public interface CompanyService {
    
    CompanyDto create(CompanyCreateRequest companyCreateRequest);
    CompanyDto getOne(Long id);
    Set<CompanyDto> getAllByPage(Integer page, Integer quantity, CompanySortBy companySortBy, SortType sortType);
    Set<CompanyDto> getAllByLegalForm(String userEmail, LegalForm legalForm, Integer page, Integer quantity, CompanySortBy companySortBy, SortType sortType);
    
}
