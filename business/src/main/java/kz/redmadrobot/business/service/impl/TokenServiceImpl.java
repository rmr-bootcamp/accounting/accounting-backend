package kz.redmadrobot.business.service.impl;

import kz.redmadrobot.business.mapper.request.TokenCreateMapper;
import kz.redmadrobot.business.model.request.TokenCreateRequest;
import kz.redmadrobot.dao.entity.AuthDetails;
import kz.redmadrobot.dao.entity.Token;
import kz.redmadrobot.dao.enums.TokenType;
import kz.redmadrobot.dao.repository.AuthDetailsRepository;
import kz.redmadrobot.dao.repository.TokenRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class TokenServiceImpl {
    private final TokenRepository tokenRepository;
    private final TokenCreateMapper tokenCreateMapper;
    private final AuthDetailsRepository authDetailsRepository;

    public void saveUserToken(TokenCreateRequest request) {
        log.info("Creating new token: {}", request.getToken());
        Optional<AuthDetails> authDetails = authDetailsRepository.findByEmail(request.getAuthDetailsDto().email());
        Token token = tokenCreateMapper.toEntity(request);
        token.setAuthDetails(authDetails.get());
        token.setToken(request.getToken());
        token.setTokenType(TokenType.BEARER);
        token.setExpired(false);
        token.setRevoked(false);
        tokenRepository.save(token);
        log.info("Token with value='{}' created and assigned", request.getToken());
    }

    public void revokeAllUserTokens(TokenCreateRequest request) {
        log.info("Searching for user email '{}' and token '{}'", request.getAuthDetailsDto().email(), request.getToken());
        Optional<AuthDetails> authDetails = authDetailsRepository.findByEmail(request.getAuthDetailsDto().email());
        var validUserTokens = tokenRepository.findAllValidTokenByUser(authDetails.get().getId());
        if (validUserTokens.isEmpty())
            return;
        validUserTokens.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens);
    }

    public Optional<Token> getOneByToken(String token) {
        log.info("Searching for stored token '{}'", token);
        var storedToken = tokenRepository.findByToken(token)
                .orElse(null);
        if (storedToken != null) {
            storedToken.setExpired(true);
            storedToken.setRevoked(true);
            tokenRepository.save(storedToken);
            return Optional.of(storedToken);
        }
        return Optional.empty();
    }
}
