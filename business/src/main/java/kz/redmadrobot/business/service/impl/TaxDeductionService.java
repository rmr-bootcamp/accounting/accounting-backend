package kz.redmadrobot.business.service.impl;

import kz.redmadrobot.business.service.tax_calculation.company.SalaryCompanyNonResidentTaxDeductionCalculator;
import kz.redmadrobot.business.service.tax_calculation.company.SalaryCompanyResidentHandicappedTaxDeductionCalculator;
import kz.redmadrobot.business.service.tax_calculation.company.SalaryCompanyResidentPensionerTaxDeductionCalculator;
import kz.redmadrobot.business.service.tax_calculation.company.SalaryCompanyResidentTaxDeductionCalculator;
import kz.redmadrobot.business.service.tax_calculation.company.SalaryCompanyTaxDeductionCalculator;
import kz.redmadrobot.business.service.tax_calculation.employee.SalaryEmployeeNonResidentTaxDeductionCalculator;
import kz.redmadrobot.business.service.tax_calculation.employee.SalaryEmployeeResidentHandicappedTaxDeductionCalculator;
import kz.redmadrobot.business.service.tax_calculation.employee.SalaryEmployeeResidentPensionerHandicappedTaxDeductionCalculator;
import kz.redmadrobot.business.service.tax_calculation.employee.SalaryEmployeeResidentPensionerTaxDeductionCalculator;
import kz.redmadrobot.business.service.tax_calculation.employee.SalaryEmployeeResidentTaxDeductionCalculator;
import kz.redmadrobot.business.service.tax_calculation.employee.SalaryEmployeeTaxDeductionCalculator;
import kz.redmadrobot.dao.entity.Employee;
import kz.redmadrobot.dao.entity.TaxDeduction;
import kz.redmadrobot.dao.enums.ResidentStatus;
import kz.redmadrobot.dao.enums.SocialStatus;
import kz.redmadrobot.dao.enums.Tax;
import kz.redmadrobot.dao.repository.TaxDeductionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
@Service
public class TaxDeductionService {

    private final TaxDeductionRepository taxDeductionRepository;

    public TaxDeduction create(TaxDeduction newTaxDeduction) {
        log.info("Create new tax deduction of '{}' tax", newTaxDeduction.getTax().getName());
        return taxDeductionRepository.save(newTaxDeduction);
    }


    /**
     * Расчет зарплатного налога
     *
     * @param deductionCalculator - калькулятор расчета налога
     * @param salary - оклад
     * @param tax - налог
     *
     * @return - налоговое отчисление
     */
    public TaxDeduction calculateSalaryTax(SalaryEmployeeTaxDeductionCalculator deductionCalculator, Employee employee, BigDecimal salary, Tax tax) {
        log.info("Calculate salary tax with name '{}' for employee with id '{}'", tax.getName(), employee.getId());
        if (tax.equals(Tax.IPN)) {
            throw new IllegalArgumentException("Use calculateIPN for calculate IPN deduction!");
        }

        return switch (tax) {
            case VOSMS -> deductionCalculator.calculateVOSMS(salary);
            case OPV -> deductionCalculator.calculateOPV(salary);
            default -> throw new IllegalArgumentException(String.format("Unexpected tax: '%s'", tax.getName()));
        };
    }



    /**
     * Вычисление Индивидуального Подоходного Налога (ИПН)
     * Для вычисления ИПН необходимы вычисления по налогам:
     *      * ВОСМС - взносы на обязательное социальное медицинское страхование
     *      * ОПВ - Обязательные Пенсионные Вычеты
     * Формула вычисления ИПН:
     *      ИПН = (Заработная_плата - (ВОСМС + ОПВ)) * процентная_ставка_ИПН
     *
     * @param deductionCalculator - калькулятор расчета налога
     * @param salary заработная плата
     * @param ipn объект налога (Tax)
     * @param vosms налоговое отчисление налога ВОСМС
     * @param opv налоговое отчисление налога ОПВ
     *
     * @return возвращает налоговое отчисление ИПН
     */
    public TaxDeduction calculateIPN(SalaryEmployeeTaxDeductionCalculator deductionCalculator, BigDecimal salary, Tax ipn, TaxDeduction vosms, TaxDeduction opv) {
        log.info("Calculate IPN for tax: '{}' with tax deducions of '{}, {}'",
                ipn.getName(), vosms.getTax().getName(), opv.getTax().getName());
        if (!ipn.equals(Tax.IPN)
                || !vosms.getTax().equals(Tax.VOSMS)
                || !opv.getTax().equals(Tax.OPV)
        ) {
            throw new IllegalArgumentException("Invalid tax or tax deductions");
        }

        TaxDeduction taxDeduction = deductionCalculator.calculateIPN(salary, vosms.getAmount(), opv.getAmount());

        log.info("IPN calculated successful");
        return taxDeduction;
    }

    /**
     * Вычисление Социальных отчислений (СО)
     *
     * @param companyTaxDeductionCalculator - калькулятор расчета налога
     * @param grossSalary - оклад
     * @param opvDeduction - отчисление Обязательных Пенсионных Взносов
     *
     * @return налоговое отчисление СО
     */
    public TaxDeduction calculateSO(
            SalaryCompanyTaxDeductionCalculator companyTaxDeductionCalculator,
            BigDecimal grossSalary,
            TaxDeduction opvDeduction
    ){
        return companyTaxDeductionCalculator.calculateSO(grossSalary, opvDeduction);
    }

    /**
     * Вычисление Отчисления на обязательное социальное медицинское страхование (ОСМС)
     *
     * @param companyTaxDeductionCalculator - калькулятор расчета налога
     * @param grossSalary - оклад
     * @return налоговое отчисление ОСМС
     */
    public TaxDeduction calculateOSMS(
            SalaryCompanyTaxDeductionCalculator companyTaxDeductionCalculator,
            BigDecimal grossSalary
    ) {
        return companyTaxDeductionCalculator.calculateOSMS(grossSalary);
    }

    /**
     * Возвращает калькулятор расчета налоговых вычетов сотрудника согласно
     * статусу резидентства и социальных статусов(при наличии)
     *
     * @param employee - сотрудник
     * @return калькулятор расчета налога
     */
    public SalaryEmployeeTaxDeductionCalculator getEmployeeTaxCalculator(Employee employee) {
        SalaryEmployeeTaxDeductionCalculator deductionCalculator = null;

        ResidentStatus residentStatus = employee.getResidentStatus();
        if (residentStatus.equals(ResidentStatus.RESIDENT)) {
            Set<SocialStatus> socialStatuses = employee.getSocialStatuses();
            if (socialStatuses.isEmpty()) {
                deductionCalculator = new SalaryEmployeeResidentTaxDeductionCalculator();
            } else if (socialStatuses.contains(SocialStatus.PENSIONER)) {
                deductionCalculator = new SalaryEmployeeResidentPensionerTaxDeductionCalculator();
            } else if (socialStatuses.contains(SocialStatus.HANDICAPPED)) {
                deductionCalculator = new SalaryEmployeeResidentHandicappedTaxDeductionCalculator();
            } else if (socialStatuses.contains(SocialStatus.PENSIONER) && socialStatuses.contains(SocialStatus.HANDICAPPED)) {
                deductionCalculator = new SalaryEmployeeResidentPensionerHandicappedTaxDeductionCalculator();
            }
        } else if (residentStatus.equals(ResidentStatus.NON_RESIDENT)) {
            deductionCalculator = new SalaryEmployeeNonResidentTaxDeductionCalculator();
        } else {
            throw new RuntimeException("Unexpected ResidentStatus");
        }

        return deductionCalculator;
    }

    /**
     * Возвращает калькулятор расчета налоговых вычетов компании согласно
     * статусу резидентства и социальных статусов(при наличии) сотрудника
     *
     * @param employee - сотрудник
     * @return калькулятор расчета налога
     */
    public SalaryCompanyTaxDeductionCalculator getSalaryCompanyTaxCalculator(Employee employee) {
        SalaryCompanyTaxDeductionCalculator deductionCalculator = null;

        ResidentStatus residentStatus = employee.getResidentStatus();
        if (residentStatus.equals(ResidentStatus.RESIDENT)) {
            Set<SocialStatus> socialStatuses = employee.getSocialStatuses();
            if (socialStatuses.isEmpty()) {
                deductionCalculator = new SalaryCompanyResidentTaxDeductionCalculator();
            } else if (socialStatuses.contains(SocialStatus.PENSIONER)) {
                deductionCalculator = new SalaryCompanyResidentPensionerTaxDeductionCalculator();
            } else if (socialStatuses.contains(SocialStatus.HANDICAPPED)) {
                deductionCalculator = new SalaryCompanyResidentHandicappedTaxDeductionCalculator();
            }
        } else if (residentStatus.equals(ResidentStatus.NON_RESIDENT)) {
            deductionCalculator = new SalaryCompanyNonResidentTaxDeductionCalculator();
        } else {
            throw new RuntimeException("Unexpected ResidentStatus");
        }

        return deductionCalculator;
    }

}
