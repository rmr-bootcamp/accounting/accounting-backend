package kz.redmadrobot.business.service.tax_calculation.company;

import kz.redmadrobot.dao.entity.TaxDeduction;
import kz.redmadrobot.dao.enums.Tax;

import java.math.BigDecimal;

public class SalaryCompanyResidentHandicappedTaxDeductionCalculator extends SalaryCompanyResidentTaxDeductionCalculator {

    @Override
    public TaxDeduction calculateOSMS(BigDecimal grossSalary) {
        TaxDeduction taxDeduction = new TaxDeduction();
        taxDeduction.setTax(Tax.OSMS);
        taxDeduction.setAmount(0L);
        return taxDeduction;
    }
}
