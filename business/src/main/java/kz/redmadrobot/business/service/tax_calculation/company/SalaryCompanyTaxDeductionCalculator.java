package kz.redmadrobot.business.service.tax_calculation.company;

import kz.redmadrobot.dao.entity.TaxDeduction;
import kz.redmadrobot.dao.enums.Tax;

import java.math.BigDecimal;

public interface SalaryCompanyTaxDeductionCalculator {

    default TaxDeduction calculateSO(BigDecimal grossSalary, TaxDeduction ipnDeduction) {
        TaxDeduction taxDeduction = new TaxDeduction();
        Tax so = Tax.SO;
        taxDeduction.setTax(so);
        long amount = grossSalary.subtract(BigDecimal.valueOf(ipnDeduction.getAmount()))
                .multiply(so.getInterestRate())
                .longValue();

        //TODO перенести логику в setAmount?
        BigDecimal minAmountToWithdraw = so.getMinAmountToWithdraw();
        if (minAmountToWithdraw != null) {
            amount = Math.max(minAmountToWithdraw.longValue(), amount);
        }
        BigDecimal maxAmountToWithdraw = so.getMaxAmountToWithdraw();
        if (maxAmountToWithdraw != null) {
            amount = Math.min(maxAmountToWithdraw.longValue(), amount);
        }

        taxDeduction.setAmount(amount);
        return taxDeduction;
    }

    default TaxDeduction calculateOSMS(BigDecimal grossSalary) {
        TaxDeduction taxDeduction = new TaxDeduction();
        Tax osms = Tax.OSMS;
        taxDeduction.setTax(osms);
        long amount = grossSalary.multiply(osms.getInterestRate())
                .longValue();

        BigDecimal maxAmountToWithdraw = osms.getMaxAmountToWithdraw();
        if (maxAmountToWithdraw != null) {
            amount = Math.min(maxAmountToWithdraw.longValue(), amount);
        }
        taxDeduction.setAmount(amount);
        return taxDeduction;
    }

}
