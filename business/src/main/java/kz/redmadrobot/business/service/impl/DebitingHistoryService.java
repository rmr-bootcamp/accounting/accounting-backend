package kz.redmadrobot.business.service.impl;

import kz.redmadrobot.business.exception.debitinghistory.DebitingHistoryException;
import kz.redmadrobot.business.model.enums.AttendanceType;
import kz.redmadrobot.business.service.tax_calculation.company.SalaryCompanyTaxDeductionCalculator;
import kz.redmadrobot.business.service.tax_calculation.employee.SalaryEmployeeTaxDeductionCalculator;
import kz.redmadrobot.business.utils.SalaryCalculator;
import kz.redmadrobot.dao.entity.Attendance;
import kz.redmadrobot.dao.entity.Company;
import kz.redmadrobot.dao.entity.DebitingHistory;
import kz.redmadrobot.dao.entity.Employee;
import kz.redmadrobot.dao.entity.HourlyAttendance;
import kz.redmadrobot.dao.entity.TaxDeduction;
import kz.redmadrobot.dao.enums.Tax;
import kz.redmadrobot.dao.repository.AttendanceRepository;
import kz.redmadrobot.dao.repository.DebitingHistoryRepository;
import kz.redmadrobot.dao.repository.HourlyAttendanceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
@Service
public class DebitingHistoryService {

    private final DebitingHistoryRepository debitingHistoryRepository;
    private final TaxDeductionService taxDeductionService;

    private final AttendanceRepository attendanceRepository;

    private final HourlyAttendanceRepository hourlyAttendanceRepository;

    private final SalaryCalculator salaryCalculator;


    /**
     * Создание объекта отчета сотрудника компании
     *
     * расчитать оклад за день
     * получить количество потенциальных рабочих дней
     * получить потенциальный оклад = оклад за день * кол-во потенциальных рабочих дней
     * получить оклад по активностям = сумма amount каждой из активностей с тем же периодом
     * получить общую сумму поощрений и/или штрафов
     * получить фактический оклад = потенциальный оклад + оклад по активностям + сумма поощрений/штрафов
     *
     * вычислить ВОСМС
     * вычислить ОПВ
     * вычислить ИПН
     * получить net ЗП
     *
     * вычислить СО
     * вычислить ОСМС
     *
     * @param company - компания
     * @param employee - сотрудник
     * @param date - дата содержащая месяц и год охватываемый отчетом
     * @return объект отчета по сотруднику компании
     */
    public DebitingHistory create(Company company, Employee employee, LocalDate date) {
        if (!Objects.equals(employee.getCompany().getId(), company.getId())) {
            throw new DebitingHistoryException(
                    String.format(
                            "Cannot create DebitingHistory: the employee with id %d is not employed by the company with id %d",
                            employee.getId(), company.getId()
                    ));
        }

        LocalDate currentDate = LocalDate.now();
        LocalDate reportEnd = date.with(TemporalAdjusters.lastDayOfMonth());
        if (date.getYear() == currentDate.getYear() && date.getMonth().equals(currentDate.getMonth())) {
            reportEnd = currentDate;
        }


        LocalDate startDate = date;


        // оклад в день
        BigDecimal grossSalaryPerDay = salaryCalculator.calculateSalaryForOneDay(startDate, employee.getGrossSalary());


        // Если сотрудник был нанят позже начала месяца, сдвигаем дату отчета на день найма
        if (startDate.isBefore(employee.getHiredAt())) {
            startDate = employee.getHiredAt();
        }


        //потенциальные рабочие дни
        int potentialWorkdayCount = salaryCalculator.weekdaysCountBetween( startDate, reportEnd );
        // потенциальный оклад
        BigDecimal potentialGrossSalary = grossSalaryPerDay.multiply(BigDecimal.valueOf(potentialWorkdayCount)).setScale(
                0, RoundingMode.HALF_EVEN
        );

        // получаем оклад по активностям
        List<Attendance> attendances = attendanceRepository.findAttendanceByDateAndEmployeeId(
                startDate, reportEnd, employee.getId());
        BigDecimal attendanceAmountSum = BigDecimal.ZERO;
        int employeeInactiveDayCount = 0;
        Set<String> inactiveAttendanceType = Set.of(
                AttendanceType.ABSENT.name(),
                AttendanceType.UNPAID_VACATION.name()
        );
        for (Attendance attendance : attendances) {
            attendanceAmountSum = attendanceAmountSum.add(attendance.getAmount());
            boolean isInactiveAttendance = inactiveAttendanceType.contains(attendance.getType());
            if (isInactiveAttendance) {
                employeeInactiveDayCount += salaryCalculator.weekdaysCountBetween(
                        attendance.getReportedStartDate(),
                        attendance.getReportedEndDate()
                );
            }
        }

        // фактически отработанные дни
        int employeeWeekdaysCount = potentialWorkdayCount - employeeInactiveDayCount;

        Set<HourlyAttendance> hourlyAttendances = hourlyAttendanceRepository.findByEmployeeAndReportedDateBetween(
                employee, startDate, reportEnd);

        BigDecimal hourlyAttendanceTotalAmount = hourlyAttendances.stream()
                .map(HourlyAttendance::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal employeeGrossSalary = employee.getGrossSalary();
        // фактический оклад за месяц
        BigDecimal actualGrossSalary = potentialGrossSalary.add(attendanceAmountSum)
                .add(hourlyAttendanceTotalAmount);

        SalaryEmployeeTaxDeductionCalculator employeeTaxCalculator = taxDeductionService.getEmployeeTaxCalculator(employee);
        TaxDeduction VOSMSDeduction = taxDeductionService.calculateSalaryTax(
                employeeTaxCalculator, employee, actualGrossSalary, Tax.VOSMS);
        TaxDeduction OPVDeduction = taxDeductionService.calculateSalaryTax(
                employeeTaxCalculator, employee, actualGrossSalary, Tax.OPV);
        TaxDeduction IPNDeduction = taxDeductionService.calculateIPN(
                employeeTaxCalculator, actualGrossSalary, Tax.IPN, VOSMSDeduction, OPVDeduction);

        List<TaxDeduction> employeeDeductionList = List.of(VOSMSDeduction, OPVDeduction, IPNDeduction);


        BigDecimal employeeTaxTotal = BigDecimal.ZERO;
        Set<TaxDeduction> taxDeductions = new HashSet<>();
        for (TaxDeduction taxDeduction : employeeDeductionList) {
            Long amount = taxDeduction.getAmount();
            if (amount > 0) {
                TaxDeduction saved = taxDeductionService.create(taxDeduction);
                taxDeductions.add(saved);
                employeeTaxTotal = employeeTaxTotal.add(BigDecimal.valueOf(amount));
            }
        }
        BigDecimal netSalaryByMonth = actualGrossSalary.subtract(employeeTaxTotal);

        SalaryCompanyTaxDeductionCalculator companyTaxDeductionCalculator = taxDeductionService.getSalaryCompanyTaxCalculator(employee);
        TaxDeduction SODeduction = taxDeductionService.calculateSO(companyTaxDeductionCalculator, actualGrossSalary, OPVDeduction);
        TaxDeduction OSMSDeduction = taxDeductionService.calculateOSMS(companyTaxDeductionCalculator, actualGrossSalary);

        BigDecimal companyTaxesTotal = BigDecimal.ZERO;
        List<TaxDeduction> companyDeductions = List.of(SODeduction, OSMSDeduction);
        for (TaxDeduction taxDeduction : companyDeductions) {
            Long amount = taxDeduction.getAmount();
            if (amount > 0) {
                TaxDeduction saved = taxDeductionService.create(taxDeduction);
                taxDeductions.add(saved);
                companyTaxesTotal = companyTaxesTotal.add(BigDecimal.valueOf(amount));
            }
        }

        DebitingHistory debitingHistory = new DebitingHistory();
        debitingHistory.setReportStartDate(startDate);
        debitingHistory.setReportEndDate(reportEnd);
        debitingHistory.setCompany(company);
        debitingHistory.setEmployee(employee);
        debitingHistory.setEmployeeFirstName(employee.getFirstName());
        debitingHistory.setEmployeeLastName(employee.getLastName());
        debitingHistory.setEmployeePatronymic(employee.getPatronymic());
        debitingHistory.setEmployeeWeekdaysCount(employeeWeekdaysCount);
        debitingHistory.setGrossSalary(employeeGrossSalary);
        debitingHistory.setActualSalary(actualGrossSalary);
        debitingHistory.setNetSalary(netSalaryByMonth);
        debitingHistory.setTaxDeductions(taxDeductions);
        debitingHistory.setEmployeeTaxesTotal(employeeTaxTotal);
        debitingHistory.setCompanyTaxesTotal(companyTaxesTotal);

        return debitingHistoryRepository.save(debitingHistory);
    }

}

