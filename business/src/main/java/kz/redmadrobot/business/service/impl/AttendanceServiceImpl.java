package kz.redmadrobot.business.service.impl;

import jakarta.persistence.EntityNotFoundException;
import kz.redmadrobot.business.exception.attendance.AttendanceDateException;
import kz.redmadrobot.business.exception.attendance.AttendanceIsExistsException;
import kz.redmadrobot.business.exception.attendance.AttendanceNotFoundException;
import kz.redmadrobot.business.mapper.dto.AttendanceMapper;
import kz.redmadrobot.business.mapper.request.AttendanceCreateMapper;
import kz.redmadrobot.business.model.dto.AttendanceDto;
import kz.redmadrobot.business.model.enums.AttendanceType;
import kz.redmadrobot.business.model.request.AttendanceCreateRequest;
import kz.redmadrobot.business.service.AttendanceService;
import kz.redmadrobot.business.utils.SalaryCalculator;
import kz.redmadrobot.dao.entity.Attendance;
import kz.redmadrobot.dao.entity.Employee;
import kz.redmadrobot.dao.repository.AttendanceRepository;
import kz.redmadrobot.dao.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class AttendanceServiceImpl implements AttendanceService {

    private final AttendanceCreateMapper attendanceCreateMapper;
    private final AttendanceMapper attendanceMapper;

    private final AttendanceRepository attendanceRepository;
    private final EmployeeRepository employeeRepository;
    private final SalaryCalculator salaryCalculator;

    @Override
    public AttendanceDto create(Long employeeId, AttendanceCreateRequest attendanceCreateRequest) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Employee with id '%s' not found!", employeeId)));

        AttendanceType attendanceType = attendanceCreateRequest.getType();
        LocalDate attendanceReportedStartDate = attendanceCreateRequest.getReportedStartDate();
        LocalDate attendanceReportedEndDate = attendanceCreateRequest.getReportedEndDate();
        LocalDate hiredAtDate = employee.getHiredAt();

        List<Attendance> attendanceList = attendanceRepository.findAttendanceByDateAndEmployeeId(attendanceReportedStartDate,
                attendanceReportedEndDate, employee.getId());

        if (attendanceReportedStartDate.isAfter(attendanceReportedEndDate)) {
            throw new AttendanceDateException(String.format("Attendance with start date '%s' comes after end date '%s'!",
                    attendanceReportedStartDate, attendanceReportedEndDate));
        }
        LocalDate currentDate = LocalDate.now();
        if (attendanceReportedStartDate.getMonth() != currentDate.getMonth() ||
                attendanceReportedEndDate.getMonth() != currentDate.getMonth()) {
            throw new AttendanceDateException("Attendance must be set to the current month");
        }
        if (!attendanceList.isEmpty()) {
            throw new AttendanceDateException(String.format("Attendance between start date '%s' and end date '%s' already exists!",
                    attendanceReportedStartDate, attendanceReportedEndDate));
        }
        if(attendanceReportedStartDate.isBefore(hiredAtDate) || attendanceReportedEndDate.isBefore(hiredAtDate)) {
            throw new AttendanceDateException(String.format("Attendance between start date '%s' or end date '%s' is before hire date" +
                            "of employee",
                    attendanceReportedStartDate, attendanceReportedEndDate));
        }

        Attendance attendance = attendanceCreateMapper.toEntity(attendanceCreateRequest);
        return getAttendanceDto(employee, attendance, attendanceType, attendanceReportedStartDate, attendanceReportedEndDate);
    }

    @Override
    public AttendanceDto getOne(Long employeeId, Long attendanceId) {
        log.info("Searching for Attendance with id='{}' and Employee with id = '{}'", attendanceId, employeeId);

        Attendance attendance = attendanceRepository.findById(attendanceId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Attendance with id '%s' not found!", attendanceId))
                );

        if (!attendance.getEmployee().getId().equals(employeeId)) {
            throw new AttendanceNotFoundException(String.format("Attendance with id='%s' does not belong to the employee id='%s'",
                    attendanceId, employeeId));
        }

        return attendanceMapper.toDto(attendance);
    }

    @Override
    public List<AttendanceDto> getAttendanceByDate(String reportedStartDate, String reportedEndDate,
                                                   Long employeeId) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Employee with id '%s' not found!", employeeId)));


        LocalDate startDate = LocalDate.parse(reportedStartDate, DateTimeFormatter.ISO_LOCAL_DATE);
        LocalDate endDate = LocalDate.parse(reportedEndDate, DateTimeFormatter.ISO_LOCAL_DATE);


        List<Attendance> attendances = attendanceRepository.findAttendanceByDateAndEmployeeId(startDate, endDate,
                employee.getId());

        return attendanceMapper.toDtoList(attendances);
    }

    @Override
    public AttendanceDto update(Long employeeId, Long attendanceId, AttendanceCreateRequest attendanceCreateRequest) {
        log.info("Updating Attendance with id='{}' and Employee with id = '{}'", attendanceId, employeeId);
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Employee with id '%s' not found!", employeeId)));
        Attendance attendance = attendanceRepository.findById(attendanceId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Attendance with id '%s' not found!", attendanceId))
                );

        if (!attendance.getEmployee().getId().equals(employee.getId())) {
            throw new AttendanceNotFoundException(String.format("Attendance with id='%s' does not belong to the employee id='%s'",
                    attendanceId, employeeId));
        }

        AttendanceType attendanceType = attendanceCreateRequest.getType();
        LocalDate attendanceReportedStartDate = attendanceCreateRequest.getReportedStartDate();
        LocalDate attendanceReportedEndDate = attendanceCreateRequest.getReportedEndDate();
        LocalDate hiredAtDate = employee.getHiredAt();

        List<Attendance> attendanceList = attendanceRepository.findAttendanceByDateAndEmployeeId(attendanceReportedStartDate,
                attendanceReportedEndDate, employee.getId());

        if (attendanceReportedStartDate.isAfter(attendanceReportedEndDate)) {
            throw new AttendanceDateException(String.format("Attendance with start date '%s' comes after end date '%s'!",
                    attendanceReportedStartDate, attendanceReportedEndDate));
        }
        LocalDate currentDate = LocalDate.now();
        if (attendanceReportedStartDate.getMonth() != currentDate.getMonth() ||
                attendanceReportedEndDate.getMonth() != currentDate.getMonth()) {
            throw new AttendanceDateException("Attendance must be set to the current month");
        }
        if (!attendance.getType().equalsIgnoreCase(attendanceCreateRequest.getType().getAttribute())) {
            throw new AttendanceIsExistsException("Attendance with another type already exists");
        }
        if (attendanceList.size() > 1) {
            throw new AttendanceIsExistsException("Attendance with another date already exists");
        }
        if(attendanceReportedStartDate.isBefore(hiredAtDate) || attendanceReportedEndDate.isBefore(hiredAtDate)) {
            throw new AttendanceDateException(String.format("Attendance between start date '%s' and end date '%s' is before hire date" +
                            "of employee",
                    attendanceReportedStartDate, attendanceReportedEndDate));
        }

        attendance.setType(attendanceType.getAttribute().toUpperCase());
        return getAttendanceDto(employee, attendance, attendanceType, attendanceReportedStartDate, attendanceReportedEndDate);
    }

    @Override
    public void deleteById(Long employeeId, Long attendanceId) {
        if (!employeeRepository.existsById(employeeId)) {
            throw new EntityNotFoundException("Employee with id '%d' not found".formatted(employeeId));
        }
        if (!attendanceRepository.existsById(attendanceId)) {
            throw new EntityNotFoundException("Attendance with id '%d' not found".formatted(attendanceId));
        }

        LinkedList<Long> employeeAttendanceIdList = attendanceRepository.getIdsByEmployeeId(employeeId);
        if (!employeeAttendanceIdList.contains(attendanceId)) {
            throw new AttendanceNotFoundException(
                    "Attendance with id '%d' is not belong to employee with id '%d'".formatted(attendanceId, employeeId));

        }

        attendanceRepository.deleteById(attendanceId);
    }

    private AttendanceDto getAttendanceDto(Employee employee, Attendance attendance, AttendanceType attendanceType, LocalDate attendanceReportedStartDate, LocalDate attendanceReportedEndDate) {
        attendance.setEmployee(employee);
        BigDecimal grossSalary = attendance.getEmployee().getGrossSalary();
        int amountOfAttendanceDays = salaryCalculator.weekdaysCountBetween(attendanceReportedStartDate, attendanceReportedEndDate);
        BigDecimal oneDayOfEmployeeSalary = salaryCalculator.calculateSalaryForOneDay(attendanceReportedStartDate, grossSalary);
        BigDecimal amountSalaryByAttendance = salaryCalculator.calculateAmountOfSalaryByAttendanceStatusType(attendanceType, grossSalary,
                oneDayOfEmployeeSalary, amountOfAttendanceDays);

        attendance.setReportedStartDate(attendanceReportedStartDate);
        attendance.setReportedEndDate(attendanceReportedEndDate);
        attendance.setAmount(amountSalaryByAttendance);

        attendanceRepository.save(attendance);

        return attendanceMapper.toDto(attendance);
    }
}
