package kz.redmadrobot.business.service.tax_calculation.employee;

import kz.redmadrobot.dao.entity.TaxDeduction;
import kz.redmadrobot.dao.enums.Tax;

import java.math.BigDecimal;
import java.math.RoundingMode;

public interface SalaryEmployeeTaxDeductionCalculator {

    default TaxDeduction calculateVOSMS(BigDecimal salary) {
        TaxDeduction taxDeduction = new TaxDeduction();
        taxDeduction.setTax(Tax.VOSMS);
        long amount = calculateTaxAmount(Tax.VOSMS, salary);
        taxDeduction.setAmount(amount);
        return taxDeduction;
    }
    default TaxDeduction calculateOPV(BigDecimal salary) {
        TaxDeduction taxDeduction = new TaxDeduction();
        taxDeduction.setTax(Tax.OPV);

        long amount = calculateTaxAmount(Tax.OPV, salary);
        taxDeduction.setAmount(amount);
        return taxDeduction;
    }

    default TaxDeduction calculateIPN(BigDecimal salary, Long vosmsAmount, Long opvAmount) {
        TaxDeduction taxDeduction = new TaxDeduction();
        Tax ipn = Tax.IPN;
        taxDeduction.setTax(ipn);
        long amount = salary
                .subtract(BigDecimal.valueOf(vosmsAmount + opvAmount))
                .multiply(ipn.getInterestRate())
                .longValue();
        taxDeduction.setAmount(amount);
        return taxDeduction;
    }

    private long calculateTaxAmount(Tax tax, BigDecimal salary) {
        long amount = salary.multiply(tax.getInterestRate())
                .setScale(0, RoundingMode.HALF_EVEN)
                .longValue();

        BigDecimal maxAmountToWithdraw = tax.getMaxAmountToWithdraw();
        if (maxAmountToWithdraw != null) {
            amount = Math.min(amount, maxAmountToWithdraw.longValue());
        }

        return amount;
    }

}
