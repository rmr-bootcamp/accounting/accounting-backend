package kz.redmadrobot.business.service.tax_calculation.employee;

import kz.redmadrobot.dao.entity.TaxDeduction;
import kz.redmadrobot.dao.enums.Tax;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class SalaryEmployeeResidentHandicappedTaxDeductionCalculator extends SalaryEmployeeResidentTaxDeductionCalculator {


    @Override
    public TaxDeduction calculateVOSMS(BigDecimal salary) {
        TaxDeduction taxDeduction = new TaxDeduction();
        taxDeduction.setTax(Tax.VOSMS);
        taxDeduction.setAmount(0L);
        return taxDeduction;
    }
}
