package kz.redmadrobot.business.service;

import kz.redmadrobot.business.model.dto.EmployeeDto;
import kz.redmadrobot.business.model.enums.sort.EmployeeSortBy;
import kz.redmadrobot.business.model.enums.sort.SortType;
import kz.redmadrobot.business.model.request.EmployeeCreateRequest;

import java.util.Set;

public interface EmployeeService {

    EmployeeDto create(EmployeeCreateRequest employeeCreateRequest);
    EmployeeDto getOne(Long id);
    Set<EmployeeDto> getAllByPage(Integer page, Integer quantity, EmployeeSortBy companySortBy, SortType sortType, Long companyId);

    void deleteById(Long id);
}
