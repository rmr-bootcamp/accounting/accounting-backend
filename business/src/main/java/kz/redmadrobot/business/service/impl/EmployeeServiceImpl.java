package kz.redmadrobot.business.service.impl;

import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import kz.redmadrobot.business.exception.attendance.AttendanceNotFoundException;
import kz.redmadrobot.business.exception.employee.EmployeeMinimumWageException;
import kz.redmadrobot.business.mapper.dto.EmployeeMapper;
import kz.redmadrobot.business.mapper.request.EmployeeCreateMapper;
import kz.redmadrobot.business.model.dto.EmployeeDto;
import kz.redmadrobot.business.model.enums.sort.EmployeeSortBy;
import kz.redmadrobot.business.model.enums.sort.SortType;
import kz.redmadrobot.business.model.request.EmployeeCreateRequest;
import kz.redmadrobot.business.service.EmployeeService;
import kz.redmadrobot.dao.entity.Company;
import kz.redmadrobot.dao.entity.Employee;
import kz.redmadrobot.dao.repository.CompanyRepository;
import kz.redmadrobot.dao.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeMapper employeeMapper;
    private final EmployeeCreateMapper employeeCreateMapper;
    private final EmployeeRepository employeeRepository;
    private final CompanyRepository companyRepository;

    public EmployeeDto create(EmployeeCreateRequest employeeCreateRequest) {
        String email = employeeCreateRequest.getEmail();
        BigDecimal minimumWageOfSalary = new BigDecimal("70000");
        log.info("Creating employee with email='{}'", email);

        if (employeeRepository.existsByEmail(email)) {
            throw new EntityExistsException(String.format("Employee with email '%s' exists!", email));
        }

        Long companyId = employeeCreateRequest.getCompanyId();
        BigDecimal grossSalary = employeeCreateRequest.getGrossSalary();
        Company company = companyRepository.findById(companyId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Company with id '%s' not found!", companyId)));


        Employee employee = employeeCreateMapper.toEntity(employeeCreateRequest);
        if (grossSalary.compareTo(minimumWageOfSalary) >= 0) {
            employee.setGrossSalary(grossSalary);
        } else {
            throw new EmployeeMinimumWageException("An employee's salary should not be less than 70,000");
        }

        employee.setCompany(company);
        employee.setSocialStatuses(employeeCreateRequest.getSocialStatuses());

        employeeRepository.save(employee);

        log.info("Employee created and saved to database with id={}", employee.getId());

        return employeeMapper.toDto(employee);
    }

    @Override
    public EmployeeDto getOne(Long id) {
        log.info("Searching for Employee with id={}", id);
        return employeeMapper.toDto(employeeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Employee with id '%s' not found!", id))));
    }

    @Override
    public Set<EmployeeDto> getAllByPage(Integer page, Integer quantity, EmployeeSortBy employeeSortBy, SortType sortType, Long companyId) {
        Company company = companyRepository.findById(companyId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Company with id '%s' not found!", companyId)));
        Sort sort = Sort.by(sortType.getDirection(), employeeSortBy.getAttribute());
        Pageable pageable = PageRequest.of(page, quantity, sort);
        Page<Employee> employeePage = employeeRepository.findAllByCompanyId(company.getId(), pageable);
        Set<Employee> employees = employeePage.toSet();

        return employeeMapper.toDtoSet(employees);
    }

    @Override
    public void deleteById(Long id) {
        if (!employeeRepository.existsById(id)) {
            throw new EntityNotFoundException("Employee with id '%s' not found".formatted(id));
        }
        employeeRepository.deleteById(id);
    }
}
