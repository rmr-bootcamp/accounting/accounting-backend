package kz.redmadrobot.business.service.impl;

import jakarta.persistence.EntityNotFoundException;
import kz.redmadrobot.business.mapper.dto.AuthDetailsMapper;
import kz.redmadrobot.business.mapper.request.AuthDetailsCreateMapper;
import kz.redmadrobot.business.model.dto.AuthDetailsDto;
import kz.redmadrobot.business.model.dto.IssuerDto;
import kz.redmadrobot.business.model.dto.SignersDto;
import kz.redmadrobot.business.model.request.RegistrationRequest;
import kz.redmadrobot.business.model.response.AuthDetailsResponse;
import kz.redmadrobot.dao.entity.AuthDetails;
import kz.redmadrobot.dao.repository.AuthDetailsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthDetailsServiceImpl {
    private final AuthDetailsRepository authDetailsRepository;
    private final AuthDetailsMapper authDetailsMapper;
    private final AuthDetailsCreateMapper authDetailsCreateMapper;

    public AuthDetailsDto getOneByEmail(String email) {
        log.info("Searching for email='{}'", email);
        return authDetailsMapper.toDto(authDetailsRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("Email not found")));
    }

    public AuthDetailsDto create(RegistrationRequest request, SignersDto signersDto) {
        log.info("Creating new user with email: {}", request.getEmail());

        AuthDetails authDetails = authDetailsCreateMapper.toEntity(request);

        AuthDetails user = setUserInformationFromEds(authDetails, signersDto);

        authDetailsRepository.save(user);

        log.info("User with email {} created and id={} assigned", user.getEmail(), user.getId());
        return authDetailsMapper.toDto(user);
    }

    private AuthDetails setUserInformationFromEds(AuthDetails authDetails, SignersDto signersDto) {
        IssuerDto issuerDto = signersDto.signers().get(0).issuer();
        authDetails.setCommonName(issuerDto.commonName());
        authDetails.setLastName(issuerDto.lastName());
        authDetails.setSurname(issuerDto.surName());
        authDetails.setOrganization(issuerDto.organization());
        authDetails.setGender(issuerDto.gender());
        authDetails.setIin(issuerDto.iin());
        authDetails.setBin(issuerDto.bin());
        authDetails.setCountry(issuerDto.country());
        authDetails.setLocality(issuerDto.locality());
        authDetails.setState(issuerDto.state());

        return authDetails;
    }
}
