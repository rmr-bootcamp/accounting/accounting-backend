package kz.redmadrobot.business.service.impl;

import com.itextpdf.html2pdf.HtmlConverter;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import kz.redmadrobot.business.exception.report.ReportException;
import kz.redmadrobot.business.utils.SalaryCalculator;
import kz.redmadrobot.dao.entity.Attendance;
import kz.redmadrobot.dao.entity.Company;
import kz.redmadrobot.dao.entity.DebitingHistory;
import kz.redmadrobot.dao.entity.Employee;
import kz.redmadrobot.dao.entity.HourlyAttendance;
import kz.redmadrobot.dao.entity.TaxDeduction;
import kz.redmadrobot.dao.enums.SocialStatus;
import kz.redmadrobot.dao.repository.AttendanceRepository;
import kz.redmadrobot.dao.repository.CompanyRepository;
import kz.redmadrobot.dao.repository.EmployeeRepository;
import kz.redmadrobot.dao.repository.HourlyAttendanceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReportService {

    private final SpringTemplateEngine springTemplateEngine;
    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;
    private final AttendanceRepository attendanceRepository;

    private final HourlyAttendanceRepository hourlyAttendanceRepository;
    private final DebitingHistoryService debitingHistoryService;
    private final SalaryCalculator salaryCalculator;
    private final String TEMPLATE_PATH = "/reports/pdf/";
    private final String DATE_TIME_PATTERN = "dd.MM.yy HH:mm:ss";

    public InputStream employeeListByCompanyId(Long id, Integer month, Integer year) {
        log.info("Processing report employeeListByCompanyId with id {}", id);
        LocalDate date = LocalDate.of(year, month, 1);

        //TODO убрать дублирование
        if (date.with(TemporalAdjusters.lastDayOfMonth()).isAfter(LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()))) {
            String message = "Cannot create report for the month that is not coming";
            log.error("employeeListByCompanyId failed: {}", message);
            throw new ReportException(message);
        }

        Context context = getEmployeeListContext(id, date);
        String reportHTML = springTemplateEngine.process(TEMPLATE_PATH + "employeeList", context);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        HtmlConverter.convertToPdf(reportHTML, outputStream);

        return new ByteArrayInputStream(outputStream.toByteArray());
    }


    public InputStream getCompanyEmployeeSalaryDetails(Long companyId, Long employeeId, @Size(min = 1, max = 12) Integer month, @Positive Integer year) {
        log.info("Processing report getCompanyEmployeeSalaryDetails with employeeId {}", employeeId);
        LocalDate date = LocalDate.of(year, month, 1);
        //TODO убрать дублирование
        if (date.with(TemporalAdjusters.lastDayOfMonth()).isAfter(LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()))) {
            String message = "Cannot create report for the month that is not coming";
            log.error("getCompanyEmployeeSalaryDetails failed: {}", message);
            throw new ReportException(message);
        }

        Context context = getEmployeeSalaryDetailsContext(companyId, employeeId, date);
        String reportHTML = springTemplateEngine.process(TEMPLATE_PATH + "employeeSalaryDetails", context);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        HtmlConverter.convertToPdf(reportHTML, outputStream);

        return new ByteArrayInputStream(outputStream.toByteArray());
    }

    private Context getEmployeeListContext(Long id, LocalDate date) {
        Company company = companyRepository.findById(id)
                .orElseThrow(() -> {
                    String message = "Company with id '%s' not found!".formatted(id);
                    log.error(message);
                    throw new EntityNotFoundException(message);
                });

        Context context = new Context();
        context.setVariable("company", company);

        LocalDate currentDate = LocalDate.now();
        LocalDate reportEnd = date.with(TemporalAdjusters.lastDayOfMonth());
        if (date.getYear() == currentDate.getYear() && date.getMonth().equals(currentDate.getMonth())) {
            reportEnd = currentDate;
        }
        List<Employee> employees = employeeRepository.findCompanyEmployeesHiredBeforeMonthAndYear(company, date.getYear(), date.getMonthValue());

        List<Map<String, Object>> reportEmployeeList = getReportEmployeeList(employees, date);

        Map<String, Object> reportMap = new HashMap<>();
        reportMap.put("period", periodAsText(date, reportEnd));
        reportMap.put("createdAt", LocalDateTime.now(ZoneId.of("UTC+06")).format(DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)));
        reportMap.put("companyTotal", calculateCompanyTotal(reportEmployeeList));

        context.setVariable("reportMap", reportMap);
        context.setVariable("employeeList", reportEmployeeList);
        return context;
    }

    private BigDecimal calculateCompanyTotal(List<Map<String, Object>> reportEmployeeList) {
        BigDecimal total = BigDecimal.ZERO;
        for (Map<String, Object> employeeReport : reportEmployeeList) {
            BigDecimal companyTotalForEmployee = (BigDecimal) employeeReport.get("companyTotalForEmployee");
            total = total.add(companyTotalForEmployee);
        }
        return total;
    }

    private Context getEmployeeSalaryDetailsContext(Long companyId, Long employeeId, LocalDate date) {
        Company company = companyRepository.findById(companyId)
                .orElseThrow(() -> {
                    String message = "Company with id '%s' not found!".formatted(companyId);
                    log.error(message);
                    throw new EntityNotFoundException(message);
                });

        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> {
                    String message = "Employee with id '%s' not found!".formatted(employeeId);
                    log.error(message);
                    throw new EntityNotFoundException(message);
                });

        Context context = new Context();

        //todo убрать дублирование
        LocalDate currentDate = LocalDate.now();
        LocalDate reportEnd = date.with(TemporalAdjusters.lastDayOfMonth());
        if (date.getYear() == currentDate.getYear() && date.getMonth().equals(currentDate.getMonth())) {
            reportEnd = currentDate;
        }


        Map<String, Object> reportMap = new HashMap<>();
        reportMap.put("period", periodAsText(date, reportEnd));
        reportMap.put("createdAt", LocalDateTime.now(ZoneId.of("UTC+06")).format(DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)));
        context.setVariable("reportMap", reportMap);

        Map<String, Object> employeeMap = new HashMap<>();
        employeeMap.put("FIO", getFio(employee));
        employeeMap.put("info", getInfo(employee));
        context.setVariable("employeeMap", employeeMap);
        if (employee.getHiredAt().isAfter(date) && employee.getHiredAt().isAfter(reportEnd)) {
            reportMap.put("unsuccedReport", "В указанный период сотрудник не состоял в штате");
            return context;
        }

        DebitingHistory debitingHistory = debitingHistoryService.create(company, employee, date);

        employeeMap.put("weekdays", debitingHistory.getEmployeeWeekdaysCount());
        employeeMap.put("salaryGross", debitingHistory.getGrossSalary().setScale(0, RoundingMode.CEILING));
        //TODO изменить имя
        employeeMap.put("salaryAttendance", debitingHistory.getActualSalary().setScale(0, RoundingMode.CEILING));
        employeeMap.put("salaryNet", debitingHistory.getNetSalary().setScale(0, RoundingMode.CEILING));


        Map<String, Object> employeeTaxesMap = new HashMap<>();
        Map<String, Object> companyTaxesMap = new HashMap<>();
        Set<TaxDeduction> taxDeductions = debitingHistory.getTaxDeductions();
        taxDeductions.forEach(taxDeduction -> {
                    Map<String, Object> taxMap = taxDeduction.getTax().isEmployeeTaxPayer() ? employeeTaxesMap : companyTaxesMap;

                    taxMap.put(taxDeduction.getTax().getName(), taxDeduction.getAmount());
                });


        Map<String, Integer> attendanceDaysByTypeMap = new LinkedHashMap<>();
        List<Attendance> attendances = attendanceRepository.findAttendanceByDateAndEmployeeId(date, reportEnd, employeeId);

        for (Attendance attendance : attendances) {
            attendanceDaysByTypeMap.merge(
                    mapAttendanceType(attendance.getType()),
                    salaryCalculator.weekdaysCountBetween(attendance.getReportedStartDate(), attendance.getReportedEndDate()),
                    Integer::sum);
        }
        attendanceDaysByTypeMap.put("итог", attendanceDaysByTypeMap.values().stream().reduce(0, Integer::sum));

        Set<HourlyAttendance> hourlyAttendances = hourlyAttendanceRepository.findByEmployeeAndReportedDateBetween(
                employee, date, reportEnd);

        List<Map<String, Object>> hourlyAttendanceList = getHourlyAttendanceInfo(hourlyAttendances);

        context.setVariable("employeeTaxMap", employeeTaxesMap);
        context.setVariable("companyTaxMap", companyTaxesMap);
        context.setVariable("attendanceDaysByTypeMap", attendanceDaysByTypeMap);
        context.setVariable("hourlyAttendanceList", hourlyAttendanceList);
        return context;
    }

    private List<Map<String, Object>> getHourlyAttendanceInfo(Set<HourlyAttendance> hourlyAttendances) {
        final long HOUR_IN_MINUTES = TimeUnit.HOURS.toMinutes(1);

        int overtimeMinutes = 0;
        int lateFineMinutes = 0;
        for(HourlyAttendance hourlyAttendance : hourlyAttendances) {
            long minutes = hourlyAttendance.getHours() * HOUR_IN_MINUTES + hourlyAttendance.getMinutes();
            switch (hourlyAttendance.getType()) {
                case OVERTIME -> overtimeMinutes += minutes;
                case LATE_FINE -> lateFineMinutes += minutes;
                default -> throw new RuntimeException("Unexpected HourlyAttendanceType");
            }
        }

        if (overtimeMinutes == 0 && lateFineMinutes == 0) {
            return List.of();
        }

        Map<String, Object> overtime = new HashMap<>();
        overtime.put("name", "Переработки");
        overtime.put("hours", overtimeMinutes / HOUR_IN_MINUTES);
        overtime.put("minutes", overtimeMinutes % HOUR_IN_MINUTES);

        Map<String, Object> lateFine = new HashMap<>();
        lateFine.put("name", "Опоздания");
        lateFine.put("hours", lateFineMinutes / HOUR_IN_MINUTES);
        lateFine.put("minutes", lateFineMinutes % HOUR_IN_MINUTES);

        return List.of(overtime, lateFine);
    }

    private String mapAttendanceType(String status) {
        return switch (status) {
            case "ABSENT" -> "отсутствовал";
            case "UNPAID_VACATION" -> "неоплачиваемый отпуск";
            case "BUSINESS_TRIP" -> "командировка";
            case "SICK_LEAVE" -> "больничный";
            case "VACATION" -> "отпуск";
            default -> "НЕИЗВЕСТНО";
        };
    }

    private String getInfo(Employee employee) {
        String residentStatus = switch (employee.getResidentStatus()) {
            case RESIDENT -> "резидент РК";
            case NON_RESIDENT -> "иностранный гражданин";
        };

        String socialStatusInfo = "отсутствуют";
        Set<SocialStatus> socialStatuses = employee.getSocialStatuses();
        if (!socialStatuses.isEmpty()) {
            socialStatusInfo = socialStatuses.stream()
                    .map(this::mapSocialStatus)
                    .collect(Collectors.joining(", "));
        }

        return String.format("статус резидентства - %s. Социальный статус - %s", residentStatus, socialStatusInfo);
    }

    private String mapSocialStatus(SocialStatus socialStatus) {
        return switch (socialStatus) {
            case PENSIONER -> "пенсионер";
            case HANDICAPPED -> "инвалид";
        };
    }

    private String periodAsText(LocalDate reportStart, LocalDate reportEnd) {
        String dateFormatPattern = "dd.MM.yy";
        return String.format(
                "с %s по %s",
                reportStart.format(DateTimeFormatter.ofPattern(dateFormatPattern)),
                reportEnd.format(DateTimeFormatter.ofPattern(dateFormatPattern))
        );
    }

    private List<Map<String, Object>> getReportEmployeeList(List<Employee> employees, LocalDate date) {

        List<Map<String, Object>> reports = new ArrayList<>();
        for (Employee employee : employees) {
            DebitingHistory debitingHistory = debitingHistoryService.create(employee.getCompany(), employee, date);
            Map<String, Object> employeeReport = new HashMap<>();
            employeeReport.put("FIO", getFio(employee));
            employeeReport.put("grossSalary", employee.getGrossSalary().setScale(0, RoundingMode.CEILING));
            BigDecimal actualGrossSalary = debitingHistory.getActualSalary().setScale(0, RoundingMode.CEILING);
            employeeReport.put("attendanceSalary", actualGrossSalary);
            //TODO Костыль. В отчете не используется, но нужен для вычисления общей суммы затрат работодателя
            employeeReport.put("companyTotalForEmployee", actualGrossSalary.add(debitingHistory.getCompanyTaxesTotal()));
            reports.add(employeeReport);
        }

        return reports;
    }

    private String getFio(Employee employee) {
        return String.format("%s %s %s",
                employee.getLastName(), employee.getFirstName(), employee.getPatronymic());
    }
}
