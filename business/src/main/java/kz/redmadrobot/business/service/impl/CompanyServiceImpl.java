package kz.redmadrobot.business.service.impl;

import jakarta.persistence.EntityNotFoundException;
import kz.redmadrobot.business.exception.global.EmailExistsException;
import kz.redmadrobot.business.exception.global.EntityNameExistsException;
import kz.redmadrobot.business.mapper.dto.CompanyMapper;
import kz.redmadrobot.business.mapper.request.CompanyCreateMapper;
import kz.redmadrobot.business.model.dto.CompanyDto;
import kz.redmadrobot.business.model.enums.LegalForm;
import kz.redmadrobot.business.model.enums.sort.CompanySortBy;
import kz.redmadrobot.business.model.enums.sort.SortType;
import kz.redmadrobot.business.model.request.CompanyCreateRequest;
import kz.redmadrobot.business.service.CompanyService;
import kz.redmadrobot.dao.entity.AuthDetails;
import kz.redmadrobot.dao.entity.Company;
import kz.redmadrobot.dao.repository.AuthDetailsRepository;
import kz.redmadrobot.dao.repository.CompanyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class CompanyServiceImpl implements CompanyService {

    private final CompanyCreateMapper companyCreateMapper;
    private final CompanyMapper companyMapper;
    private final CompanyRepository companyRepository;
    private final AuthDetailsRepository authDetailsRepository;

    @Override
    public CompanyDto create(CompanyCreateRequest companyCreateRequest) {
        String companyName = companyCreateRequest.getCompanyName();
        String corporateEmail = companyCreateRequest.getCorporateEmail();
        String userEmail = companyCreateRequest.getUserEmail();
        log.info("Creating company with name='{}' and corporateEmail='{}'", companyName,
                corporateEmail);

        if (companyRepository.existsByCompanyNameIgnoreCase(companyName)) {
            throw new EntityNameExistsException(String.format("Company with name '%s' exists!", companyName));
        }

        if (companyRepository.existsByCorporateEmailIgnoreCase(corporateEmail)) {
            throw new EmailExistsException(String.format("Company with email '%s' exists!", corporateEmail));
        }

        Company company = companyCreateMapper.toEntity(companyCreateRequest);

        Optional<AuthDetails> authDetails = authDetailsRepository.findByEmail(userEmail);
        authDetails.ifPresent(company::setAuthDetails);
        companyRepository.save(company);

        return companyMapper.toDto(company);
    }

    @Override
    public CompanyDto getOne(Long id) {
        log.info("Searching for Company with id={}", id);
        return companyMapper.toDto(companyRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException(String.format("Company with id '%d' not found!", id)))
        );
    }

    @Override
    public Set<CompanyDto> getAllByPage(Integer page, Integer quantity, CompanySortBy companySortBy, SortType sortType) {
        Sort sort = Sort.by(sortType.getDirection(), companySortBy.getAttribute());
        Pageable pageable = PageRequest.of(page, quantity, sort);
        Page<Company> companyPage = companyRepository.findAll(pageable);
        Set<Company> companies = companyPage.toSet();

        return companyMapper.toDtoSet(companies);
    }

    @Override
    public Set<CompanyDto> getAllByLegalForm(String userEmail, LegalForm legalForm, Integer page, Integer quantity, CompanySortBy companySortBy, SortType sortType) {
        Sort sort = Sort.by(sortType.getDirection(), companySortBy.getAttribute());
        Pageable pageable = PageRequest.of(page, quantity, sort);
        Page<Company> companyPage = companyRepository.findByLegalFormIgnoreCaseAndAuthDetailsEmail(userEmail, legalForm.name(), pageable);
        Set<Company> companies = companyPage.toSet();

        return companyMapper.toDtoSet(companies);
    }
}
