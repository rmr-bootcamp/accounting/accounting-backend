package kz.redmadrobot.business.service.impl;

import jakarta.persistence.EntityNotFoundException;
import kz.redmadrobot.business.exception.registration.UserIsNotValidException;
import kz.redmadrobot.business.mapper.dto.PersonalInfoMapper;
import kz.redmadrobot.business.model.dto.PersonalInfoDto;
import kz.redmadrobot.business.model.request.PersonalInfoUpdateRequest;
import kz.redmadrobot.dao.entity.AuthDetails;
import kz.redmadrobot.dao.repository.AuthDetailsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class PersonalInfoServiceImpl {
    private final AuthDetailsRepository authDetailsRepository;
    private final PersonalInfoMapper personalInfoMapper;

    public PersonalInfoDto get(String email) {
        log.info("Searching for email='{}'", email);
        AuthDetails authDetails = authDetailsRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("Email not found"));
        return personalInfoMapper.toDto(authDetails);
    }

    public PersonalInfoDto update(PersonalInfoUpdateRequest personalInfoUpdateRequest, String email) {
        Optional<AuthDetails> authDetails = authDetailsRepository.findByEmail(email);
        if (authDetails.isPresent()) {
            AuthDetails user = updatePersonalInfo(personalInfoUpdateRequest, authDetails);
            return personalInfoMapper.toDto(user);
        }
        throw new UserIsNotValidException("asdasd");
    }

    // TODO Refactor this service
    private AuthDetails updatePersonalInfo(PersonalInfoUpdateRequest personalInfoUpdateRequest, Optional<AuthDetails> authDetails) {
        AuthDetails user = authDetails.get();
        String commonName = personalInfoUpdateRequest.getCommonName();
        String lastName = personalInfoUpdateRequest.getLastName();
        String surName = personalInfoUpdateRequest.getSurName();
        String gender = personalInfoUpdateRequest.getGender();
        String country = personalInfoUpdateRequest.getCountry();
        String locality = personalInfoUpdateRequest.getLocality();
        String state = personalInfoUpdateRequest.getState();
        user.setCommonName(commonName);
        user.setLastName(lastName);
        user.setSurname(surName);
        user.setGender(gender);
        user.setCountry(country);
        user.setLocality(locality);
        user.setState(state);
        authDetailsRepository.save(user);
        return user;
    }
}
