package kz.redmadrobot.business.mapper.request;

import kz.redmadrobot.business.mapper.config.EntityMapper;
import kz.redmadrobot.business.mapper.config.MapstructConfig;
import kz.redmadrobot.business.model.request.TokenCreateRequest;
import kz.redmadrobot.dao.entity.Token;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructConfig.class)
public interface TokenCreateMapper extends EntityMapper<Token, TokenCreateRequest> {
    @Override
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "tokenType", ignore = true)
    @Mapping(target = "revoked", ignore = true)
    @Mapping(target = "expired", ignore = true)
    @Mapping(target = "authDetails", ignore = true)
    Token toEntity(TokenCreateRequest entity);
}
