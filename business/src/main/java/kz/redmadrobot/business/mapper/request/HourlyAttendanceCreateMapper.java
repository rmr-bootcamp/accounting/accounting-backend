package kz.redmadrobot.business.mapper.request;

import kz.redmadrobot.business.mapper.config.EntityMapper;
import kz.redmadrobot.business.mapper.config.MapstructConfig;
import kz.redmadrobot.business.model.enums.HourlyAttendanceType;
import kz.redmadrobot.business.model.request.HourlyAttendanceCreateRequest;
import kz.redmadrobot.dao.entity.HourlyAttendance;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

@Mapper(config = MapstructConfig.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface HourlyAttendanceCreateMapper extends EntityMapper<HourlyAttendance, HourlyAttendanceCreateRequest> {
    @Override
    HourlyAttendance toEntity(HourlyAttendanceCreateRequest dto);

    @Named("hourlyAttendanceTypeEnumToString")
    static String hourlyAttendanceTypeEnumToString(HourlyAttendanceType hourlyAttendanceType) {
        return hourlyAttendanceType.toString().trim();
    }
}
