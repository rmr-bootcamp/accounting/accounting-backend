package kz.redmadrobot.business.mapper.request;

import kz.redmadrobot.business.mapper.config.EntityMapper;
import kz.redmadrobot.business.mapper.config.MapstructConfig;
import kz.redmadrobot.dao.enums.ResidentStatus;
import kz.redmadrobot.business.model.request.EmployeeCreateRequest;
import kz.redmadrobot.dao.entity.Employee;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(config = MapstructConfig.class)
public interface EmployeeCreateMapper extends EntityMapper<Employee, EmployeeCreateRequest> {

    @Named("residentStatusEnumToString")
    static String residentStatusEnumToString(ResidentStatus residentStatus) {
        return residentStatus.toString().trim();
    }

    @Override
    @Mapping(target = "residentStatus", source = "residentStatus", qualifiedByName = "residentStatusEnumToString")
    @Mapping(target = "id", ignore = true)
    //TODO здесь точно нужен ignore?
    @Mapping(target = "company", ignore = true)
    Employee toEntity(EmployeeCreateRequest entity);
}
