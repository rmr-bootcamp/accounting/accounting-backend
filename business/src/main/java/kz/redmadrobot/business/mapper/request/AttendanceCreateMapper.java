package kz.redmadrobot.business.mapper.request;

import kz.redmadrobot.business.mapper.config.EntityMapper;
import kz.redmadrobot.business.mapper.config.MapstructConfig;
import kz.redmadrobot.business.model.enums.AttendanceType;
import kz.redmadrobot.business.model.request.AttendanceCreateRequest;
import kz.redmadrobot.dao.entity.Attendance;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(config = MapstructConfig.class)
public interface AttendanceCreateMapper extends EntityMapper<Attendance, AttendanceCreateRequest> {
    @Override
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "type", source = "type", qualifiedByName = "attendanceTypeEnumToString")
    @Mapping(target = "employee", ignore = true)
    @Mapping(target = "amount", ignore = true)
    @Mapping(target = "createDate", ignore = true)
    @Mapping(target = "modifyDate", ignore = true)
    Attendance toEntity(AttendanceCreateRequest dto);

    @Named("attendanceTypeEnumToString")
    static String attendanceTypeEnumToString(AttendanceType attendanceType) {
        return attendanceType.toString().trim();
    }
}