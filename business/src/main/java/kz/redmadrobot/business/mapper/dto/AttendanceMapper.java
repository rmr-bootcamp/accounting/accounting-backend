package kz.redmadrobot.business.mapper.dto;

import kz.redmadrobot.business.mapper.config.DtoMapper;
import kz.redmadrobot.business.mapper.config.EntityMapper;
import kz.redmadrobot.business.mapper.config.MapstructConfig;
import kz.redmadrobot.business.model.enums.AttendanceType;
import kz.redmadrobot.dao.entity.Attendance;
import kz.redmadrobot.business.model.dto.AttendanceDto;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class)
public interface AttendanceMapper extends DtoMapper<AttendanceDto, Attendance> {
    private AttendanceType stringToEnum(String name) {
        return AttendanceType.valueOf(name.toUpperCase());
    }
}