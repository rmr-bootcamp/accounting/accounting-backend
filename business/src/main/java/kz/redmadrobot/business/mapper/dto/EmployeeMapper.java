package kz.redmadrobot.business.mapper.dto;

import kz.redmadrobot.business.mapper.config.DtoMapper;
import kz.redmadrobot.business.mapper.config.MapstructConfig;
import kz.redmadrobot.business.model.dto.AttendanceDto;
import kz.redmadrobot.business.model.dto.EmployeeDto;
import kz.redmadrobot.dao.entity.Employee;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {CompanyMapper.class,
        AttendanceDto.class })
public interface EmployeeMapper extends DtoMapper<EmployeeDto, Employee> {
}