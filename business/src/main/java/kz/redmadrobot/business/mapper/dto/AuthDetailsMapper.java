package kz.redmadrobot.business.mapper.dto;

import kz.redmadrobot.business.mapper.config.DtoMapper;
import kz.redmadrobot.business.mapper.config.MapstructConfig;
import kz.redmadrobot.business.model.dto.AuthDetailsDto;
import kz.redmadrobot.dao.entity.AuthDetails;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class)
public interface AuthDetailsMapper extends DtoMapper<AuthDetailsDto, AuthDetails> {
}