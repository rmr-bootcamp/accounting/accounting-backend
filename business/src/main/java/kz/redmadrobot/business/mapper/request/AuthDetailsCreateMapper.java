package kz.redmadrobot.business.mapper.request;

import kz.redmadrobot.business.mapper.config.EntityMapper;
import kz.redmadrobot.business.mapper.config.MapstructConfig;
import kz.redmadrobot.business.model.request.RegistrationRequest;
import kz.redmadrobot.dao.entity.AuthDetails;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(config = MapstructConfig.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AuthDetailsCreateMapper extends EntityMapper<AuthDetails, RegistrationRequest> {
    @Override
    AuthDetails toEntity(RegistrationRequest dto);
}
