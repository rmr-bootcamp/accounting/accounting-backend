package kz.redmadrobot.business.mapper.dto;

import kz.redmadrobot.business.mapper.config.DtoMapper;
import kz.redmadrobot.business.mapper.config.EntityMapper;
import kz.redmadrobot.business.mapper.config.MapstructConfig;
import kz.redmadrobot.business.model.dto.CompanyDto;
import kz.redmadrobot.business.model.enums.LegalForm;
import kz.redmadrobot.dao.entity.Company;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class, uses = {EmployeeMapper.class})
public interface CompanyMapper extends DtoMapper<CompanyDto, Company> {

    private LegalForm stringToEnum(String name) {
        return LegalForm.valueOf(name.toUpperCase());
    }
}