package kz.redmadrobot.business.mapper.request;

import kz.redmadrobot.business.mapper.config.EntityMapper;
import kz.redmadrobot.business.mapper.config.MapstructConfig;
import kz.redmadrobot.business.model.enums.LegalForm;
import kz.redmadrobot.business.model.request.CompanyCreateRequest;
import kz.redmadrobot.dao.entity.Company;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructConfig.class)
public interface CompanyCreateMapper extends EntityMapper<Company, CompanyCreateRequest> {

    @Override
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "authDetails", ignore = true)
    Company toEntity(CompanyCreateRequest dto);

    private String enumToString(LegalForm legalForm) {
        return legalForm.name().trim();
    }

}
