package kz.redmadrobot.business.mapper.request;

import kz.redmadrobot.business.mapper.config.EntityMapper;
import kz.redmadrobot.business.mapper.config.MapstructConfig;
import kz.redmadrobot.business.model.request.PersonalInfoUpdateRequest;
import kz.redmadrobot.dao.entity.AuthDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(config = MapstructConfig.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PersonalInfoUpdateMapper extends EntityMapper<AuthDetails, PersonalInfoUpdateRequest> {
    @Override
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "username", ignore = true)
    @Mapping(target = "email", ignore = true)
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "surname", ignore = true)
    @Mapping(target = "organization", ignore = true)
    @Mapping(target = "iin", ignore = true)
    @Mapping(target = "bin", ignore = true)
    @Mapping(target = "tokens", ignore = true)
    AuthDetails toEntity(PersonalInfoUpdateRequest dto);
}
