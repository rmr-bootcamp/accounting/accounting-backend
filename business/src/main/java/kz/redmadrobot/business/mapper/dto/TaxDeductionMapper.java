package kz.redmadrobot.business.mapper.dto;

import kz.redmadrobot.business.mapper.config.DtoMapper;
import kz.redmadrobot.business.mapper.config.MapstructConfig;
import kz.redmadrobot.business.model.dto.TaxDeductionDto;
import kz.redmadrobot.dao.entity.TaxDeduction;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class)
public interface TaxDeductionMapper extends DtoMapper<TaxDeductionDto, TaxDeduction> {
}