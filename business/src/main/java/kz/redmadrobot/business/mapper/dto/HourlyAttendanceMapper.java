package kz.redmadrobot.business.mapper.dto;

import kz.redmadrobot.business.mapper.config.DtoMapper;
import kz.redmadrobot.business.mapper.config.MapstructConfig;
import kz.redmadrobot.business.model.dto.HourlyAttendanceDto;
import kz.redmadrobot.business.model.enums.HourlyAttendanceType;
import kz.redmadrobot.dao.entity.HourlyAttendance;
import org.mapstruct.Mapper;

@Mapper(config = MapstructConfig.class)
public interface HourlyAttendanceMapper extends DtoMapper<HourlyAttendanceDto, HourlyAttendance> {
    private HourlyAttendanceType stringToEnum(String name) {
        return HourlyAttendanceType.valueOf(name.toUpperCase());
    }
}
