package kz.redmadrobot.business;

import kz.redmadrobot.dao.DaoConfig;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootConfiguration
@ComponentScan(basePackageClasses = DaoConfig.class)
public class BusinessConfig {
}
