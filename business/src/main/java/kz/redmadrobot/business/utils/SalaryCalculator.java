package kz.redmadrobot.business.utils;

import kz.redmadrobot.business.exception.attendancehour.HourlyAttendanceExceedingLimitByHoursException;
import kz.redmadrobot.business.model.enums.AttendanceType;
import kz.redmadrobot.business.model.enums.HourlyAttendanceType;
import kz.redmadrobot.dao.entity.Calendar;
import kz.redmadrobot.dao.repository.CalendarRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class SalaryCalculator {
    private static final BigDecimal ONE_DAY_HOURS = new BigDecimal("8");
    private static final BigDecimal NEGATIVE_AMOUNT = new BigDecimal("-1");
    private static final BigDecimal MAXIMUM_HOURS_OF_OVERTIME = new BigDecimal("2");
    private static final BigDecimal MAXIMUM_HOURS_OF_LATE_FINE = new BigDecimal("8");
    private static final BigDecimal ONE_HOUR_IN_MINUTES = new BigDecimal("60");


    private final CalendarRepository calendarRepository;

    /**
     * Возвращает количество рабочих дней с учетом праздников
     *
     * @param startDate начальная дата
     * @param endDate   конечная дата (включительно)
     * @return количество рабочих дней
     */
    public int weekdaysCountBetween(LocalDate startDate, LocalDate endDate) {
        log.info("Counting weekdays - start: startDate = '{}', endDate = '{}'",
                startDate, endDate);

        int weekdays = 0;
        List<LocalDate> holidays = new ArrayList<>();
        List<LocalDate> forcedWorkdays = new ArrayList<>();
        calendarRepository.findByDateBetween(startDate, endDate).forEach(calendar -> {
            Calendar.DateType dateType = calendar.getDateType();
            if (Objects.equals(dateType, Calendar.DateType.HOLIDAY)) {
                holidays.add(calendar.getDate());
            } else if (Objects.equals(dateType, Calendar.DateType.FORCED_WORKDAY)) {
                forcedWorkdays.add(calendar.getDate());
            } else {
                throw new RuntimeException("Unexpected enum");
            }
        });

        LocalDate date = startDate;
        while (date.isBefore(endDate) || date.isEqual(endDate)) {
            DayOfWeek dayOfWeek = date.getDayOfWeek();
            boolean isNotWeekend = !holidays.contains(date) && (dayOfWeek != DayOfWeek.SATURDAY && dayOfWeek != DayOfWeek.SUNDAY);
            boolean isForcedWorkday = forcedWorkdays.contains(date);
            if (isNotWeekend || isForcedWorkday) {
                weekdays++;
            }
            date = date.plus(1, ChronoUnit.DAYS);
        }
        log.info("Counting weekdays - end: startDate = '{}', endDate = '{}', weekdays = '{}'",
                startDate, endDate, weekdays);
        return weekdays;
    }

    /**
     * Расчет заработной платы за 1 рабочий день
     *
     * @param date           - дата, содержащая месяц, для которого необходим расчет
     * @param employeeSalary - з.п. сотрудника
     * @return з.п. сотрудника за день
     */
    public BigDecimal calculateSalaryForOneDay(LocalDate date, BigDecimal employeeSalary) {
        log.info("Calculating salary for one day - start: date = '{}'",
                date);
        LocalDate startDate = LocalDate.of(date.getYear(), date.getMonth(), 1);
        LocalDate endDate = startDate.with(TemporalAdjusters.lastDayOfMonth());

        int weekdays = weekdaysCountBetween(startDate, endDate);

        BigDecimal result = employeeSalary.divide(BigDecimal.valueOf(weekdays), 2, RoundingMode.HALF_UP);
        log.info("Calculating salary for one day - end: date = '{}', result = '{}'",
                startDate, result);
        return result;
    }

    public BigDecimal calculateAmountOfSalaryByAttendanceStatusType(AttendanceType attendanceStatusType, BigDecimal grossSalary,
                                                                    BigDecimal oneDayOfEmployeeSalary, int amountOfAttendanceDays) {
        log.info("Calculating amount of salary by attendance status type - start: attendanceStatusType = '{}', grossSalary = '{}', " +
                        "oneDayOfEmployeeSalary = '{}', amountOfAttendanceDays = '{}",
                attendanceStatusType.getAttribute(), grossSalary, oneDayOfEmployeeSalary, amountOfAttendanceDays);
        if (attendanceStatusType == AttendanceType.ABSENT ||
                attendanceStatusType == AttendanceType.UNPAID_VACATION) {
            BigDecimal resultOfCalculation = oneDayOfEmployeeSalary.multiply(BigDecimal.valueOf(amountOfAttendanceDays)).multiply(NEGATIVE_AMOUNT);

            log.info("Calculating amount of salary by attendance status type - end: attendanceStatusType = '{}', grossSalary = '{}', " +
                            "oneDayOfEmployeeSalary = '{}', amountOfAttendanceDays = '{}, resultOfCalculation = '{}'",
                    attendanceStatusType.getAttribute(), grossSalary, oneDayOfEmployeeSalary, amountOfAttendanceDays, resultOfCalculation);
            return resultOfCalculation;
        }
        // TODO Реализовать остальные типы attendance-ев (Пока что вернул 0)
        return new BigDecimal("0");
    }

    /**
     * Конвертация установленных времени в часы по переработкам
     *
     * @param hours   - установленные часы
     * @param minutes - установленные минуты
     * @return данные конвертированные в часы
     */
    public BigDecimal convertHourlyAttendanceTimeToHours(Integer hours, Integer minutes) {
        log.info("Converting HourlyAttendance time to hours - start: hours = '{}', minutes = '{}', ",
                hours, minutes);

        BigDecimal attendanceHours = new BigDecimal(hours);
        BigDecimal attendanceMinutes = new BigDecimal(minutes);
        BigDecimal convertedValueFromMinutesToHours = attendanceMinutes.divide(ONE_HOUR_IN_MINUTES, 2, RoundingMode.HALF_UP);

        BigDecimal convertedHours = convertedValueFromMinutesToHours.add(attendanceHours);

        log.info("Converting HourlyAttendance time to hours - end: convertedHours = '{}",
                convertedHours);
        return convertedHours;
    }

    /**
     * Расчет заработной платы за 1 рабочий час
     *
     * @param reportedDate   - содержит дату, для которого необходим расчет
     * @param employeeSalary - з.п. сотрудника
     * @return з.п. сотрудника за 1 час
     */
    public BigDecimal calculateSalaryForOneHour(LocalDate reportedDate, BigDecimal employeeSalary) {
        log.info("Calculating salary for hourly rate - start: reportedDate = '{}', employeeSalary = '{}'",
                reportedDate, employeeSalary);

        BigDecimal oneDayOfSalary = calculateSalaryForOneDay(reportedDate, employeeSalary);
        BigDecimal hourlySalary = oneDayOfSalary.divide(ONE_DAY_HOURS, 2, RoundingMode.HALF_UP);

        log.info("Calculating salary for one day - end: reportedDate = '{}', hourlySalary = '{}', employeeSalary = '{}'",
                reportedDate, hourlySalary, employeeSalary);
        return hourlySalary;
    }

    /**
     * Общий расчет заработной платы по переработкам
     *
     * @param hourlyAttendanceType      - тип переработки (переработка, опоздание)
     * @param grossSalary             - з.п. сотрудника
     * @param oneHourOfEmployeeSalary - з.п. сотрудника за один час
     * @param convertedHours          - установленные часы за переработки
     * @return количество вычета или прибавки к з.п.
     */
    public BigDecimal calculateAmountOfSalaryByHourlyAttendanceType(HourlyAttendanceType hourlyAttendanceType, BigDecimal grossSalary,
                                                                    BigDecimal oneHourOfEmployeeSalary, BigDecimal convertedHours) {
        log.info("Calculating amount of salary by hourly attendance type - start: hourlyAttendanceType = '{}', grossSalary = '{}', " +
                        "oneDayOfEmployeeSalary = '{}', convertedHours = '{}'", hourlyAttendanceType.getAttribute(), grossSalary,
                oneHourOfEmployeeSalary, convertedHours);

        if (hourlyAttendanceType == HourlyAttendanceType.OVERTIME && convertedHours.compareTo(MAXIMUM_HOURS_OF_OVERTIME) <= 0) {
            return oneHourOfEmployeeSalary.multiply(convertedHours);
        } else if (hourlyAttendanceType == HourlyAttendanceType.LATE_FINE && convertedHours.compareTo(MAXIMUM_HOURS_OF_LATE_FINE) <= 0) {
            return oneHourOfEmployeeSalary.multiply(convertedHours).multiply(NEGATIVE_AMOUNT);
        }
        throw new HourlyAttendanceExceedingLimitByHoursException(String.format("This time '%s' exceeds the hourly attendance limit",
                convertedHours));
    }
}
