package kz.redmadrobot.business.service.impl;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class TaxDeductionServiceTest {
//
//
//    @Mock
//    TaxDeductionRepository taxDeductionRepository;
//
//    @InjectMocks
//    TaxDeductionService taxDeductionService = new TaxDeductionService(taxDeductionRepository);
//
//    Employee employeeResident;
//
//    Tax vosms;
//    Tax opv;
//    Tax ipn;
//
//    @BeforeEach
//    void init() {
//        ipn = new Tax();
//        ipn.setName(TaxName.IPN);
//        ipn.setIsEmployeeTaxPayer(true);
//        ipn.setInterestRate(BigDecimal.valueOf(0.1));
//
//        vosms = new Tax();
//        vosms.setName(TaxName.VOSMS);
//        vosms.setIsEmployeeTaxPayer(true);
//        vosms.setInterestRate(BigDecimal.valueOf(0.02));
//        vosms.setMaxAmountToWithdraw(BigDecimal.valueOf(14000L));
//
//        opv = new Tax();
//        opv.setName(TaxName.OPV);
//        opv.setIsEmployeeTaxPayer(true);
//        opv.setInterestRate(BigDecimal.valueOf(0.1));
//        opv.setMaxAmountToWithdraw(BigDecimal.valueOf(350000L));
//
//
//        employeeResident = new Employee();
//        employeeResident.setResidentStatus(ResidentStatus.RESIDENT);
//    }
//
//    @Test
//    @DisplayName("calculateIPN with IPN tax and proper tax deductions calculates successful")
//    void givenSalaryAndIPNTaxAndProperDeductions_whenCalculateIPN_thenGetIPNDeduction() {
//
//        // given
//        BigDecimal salary = BigDecimal.valueOf(200000);
//
//        TaxDeduction vosmsDeduction = new TaxDeduction();
//        vosmsDeduction.setTax(vosms);
//        vosmsDeduction.setAmount(4000L);
//
//        TaxDeduction opvDeduction = new TaxDeduction();
//        opvDeduction.setTax(opv);
//        opvDeduction.setAmount(20000L);
//
//        // when
//        TaxDeduction ipnDeduction = taxDeductionService.calculateIPN(salary, ipn, vosmsDeduction, opvDeduction);
//
//        // then
//        Assertions.assertEquals(17600L, ipnDeduction.getAmount());
//    }
//
//    //TODO добавить ParameterizedTest и MethodSource
//    @Test
//    void givenInvalidTaxAndDeductions_whenCalculateIPN_thenThrowsException() {
//        TaxDeduction taxDeduction1 = new TaxDeduction();
//        taxDeduction1.setTax(ipn);
//        TaxDeduction taxDeduction2  = new TaxDeduction();
//        taxDeduction2.setTax(ipn);
//
//        Assertions.assertThrows(
//                IllegalArgumentException.class,
//                () ->  taxDeductionService.calculateIPN(BigDecimal.valueOf(100000), opv, taxDeduction1, taxDeduction2)
//        );
//    }
//
//    //TODO добавить ParameterizedTest и MethodSource
//    @Test
//    void whenCalculateSalaryTaxWithMaxAmountToWithdraw_thenCalculatesProperly() {
//        BigDecimal highSalary = BigDecimal.valueOf(4_000_000);
//        TaxDeduction highSalaryVOSMSDeduction = taxDeductionService.calculateSalaryTax(
//                employeeResident, highSalary, vosms);
//        TaxDeduction highSalaryOPVDeduction = taxDeductionService.calculateSalaryTax(
//                employeeResident, highSalary, opv);
//
//        Assertions.assertEquals(14_000L, highSalaryVOSMSDeduction.getAmount());
//        Assertions.assertEquals(350_000L, highSalaryOPVDeduction.getAmount());
//    }
//
//    @Test
//    void givenIPNTax_whenCalculateSalaryTax_thenThrowsException() {
//        Assertions.assertThrows(
//                IllegalArgumentException.class,
//                () -> taxDeductionService.calculateSalaryTax(employeeResident, BigDecimal.valueOf(100000), ipn)
//        );
//    }
}